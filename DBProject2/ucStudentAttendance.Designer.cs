﻿
namespace DBProject2
{
    partial class ucStudentAttendance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStudentAttendance));
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbAttendanceId = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddStudentAttendance = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtMarksAttendance = new System.Windows.Forms.DataGridView();
            this.status = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.presentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.absentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshSearch = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RefreshtoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtMarksAttendance)).BeginInit();
            this.status.SuspendLayout();
            this.refreshSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(981, 264);
            this.panel1.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(144, 49);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(701, 169);
            this.splitContainer1.SplitterDistance = 640;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cmbAttendanceId);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.btnAddStudentAttendance);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(640, 169);
            this.panel3.TabIndex = 3;
            // 
            // cmbAttendanceId
            // 
            this.cmbAttendanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAttendanceId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbAttendanceId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAttendanceId.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbAttendanceId.FormattingEnabled = true;
            this.cmbAttendanceId.Location = new System.Drawing.Point(165, 39);
            this.cmbAttendanceId.Name = "cmbAttendanceId";
            this.cmbAttendanceId.Size = new System.Drawing.Size(442, 28);
            this.cmbAttendanceId.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "Attendance Date:";
            // 
            // btnAddStudentAttendance
            // 
            this.btnAddStudentAttendance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddStudentAttendance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStudentAttendance.Location = new System.Drawing.Point(24, 108);
            this.btnAddStudentAttendance.Name = "btnAddStudentAttendance";
            this.btnAddStudentAttendance.Size = new System.Drawing.Size(584, 38);
            this.btnAddStudentAttendance.TabIndex = 4;
            this.btnAddStudentAttendance.Text = "Add";
            this.btnAddStudentAttendance.UseVisualStyleBackColor = true;
            this.btnAddStudentAttendance.Click += new System.EventHandler(this.btnAddStudentAttendance_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtMarksAttendance);
            this.panel2.Location = new System.Drawing.Point(3, 263);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(981, 366);
            this.panel2.TabIndex = 3;
            // 
            // dtMarksAttendance
            // 
            this.dtMarksAttendance.AllowUserToAddRows = false;
            this.dtMarksAttendance.AllowUserToResizeColumns = false;
            this.dtMarksAttendance.AllowUserToResizeRows = false;
            this.dtMarksAttendance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtMarksAttendance.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtMarksAttendance.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtMarksAttendance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtMarksAttendance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtMarksAttendance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtMarksAttendance.ColumnHeadersHeight = 35;
            this.dtMarksAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dtMarksAttendance.ContextMenuStrip = this.status;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtMarksAttendance.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtMarksAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtMarksAttendance.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtMarksAttendance.Location = new System.Drawing.Point(0, 0);
            this.dtMarksAttendance.Name = "dtMarksAttendance";
            this.dtMarksAttendance.ReadOnly = true;
            this.dtMarksAttendance.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtMarksAttendance.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtMarksAttendance.RowHeadersVisible = false;
            this.dtMarksAttendance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtMarksAttendance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtMarksAttendance.Size = new System.Drawing.Size(981, 366);
            this.dtMarksAttendance.TabIndex = 19;
            this.dtMarksAttendance.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtMarksAttendance_CellMouseEnter);
            // 
            // status
            // 
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.presentToolStripMenuItem,
            this.absentToolStripMenuItem,
            this.leaveToolStripMenuItem,
            this.lateToolStripMenuItem});
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(130, 108);
            // 
            // presentToolStripMenuItem
            // 
            this.presentToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.presentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.presentToolStripMenuItem.Name = "presentToolStripMenuItem";
            this.presentToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.presentToolStripMenuItem.Text = "Present";
            this.presentToolStripMenuItem.Click += new System.EventHandler(this.presentToolStripMenuItem_Click);
            // 
            // absentToolStripMenuItem
            // 
            this.absentToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.absentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.absentToolStripMenuItem.Name = "absentToolStripMenuItem";
            this.absentToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.absentToolStripMenuItem.Text = "Absent";
            this.absentToolStripMenuItem.Click += new System.EventHandler(this.absentToolStripMenuItem_Click);
            // 
            // leaveToolStripMenuItem
            // 
            this.leaveToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.leaveToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.leaveToolStripMenuItem.Name = "leaveToolStripMenuItem";
            this.leaveToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.leaveToolStripMenuItem.Text = "Leave";
            this.leaveToolStripMenuItem.Click += new System.EventHandler(this.leaveToolStripMenuItem_Click);
            // 
            // lateToolStripMenuItem
            // 
            this.lateToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lateToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lateToolStripMenuItem.Name = "lateToolStripMenuItem";
            this.lateToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.lateToolStripMenuItem.Text = "Late";
            this.lateToolStripMenuItem.Click += new System.EventHandler(this.lateToolStripMenuItem_Click);
            // 
            // refreshSearch
            // 
            this.refreshSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RefreshtoolStripMenuItem1});
            this.refreshSearch.Name = "refreshAddRubric";
            this.refreshSearch.Size = new System.Drawing.Size(158, 30);
            // 
            // RefreshtoolStripMenuItem1
            // 
            this.RefreshtoolStripMenuItem1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.RefreshtoolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RefreshtoolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("RefreshtoolStripMenuItem1.Image")));
            this.RefreshtoolStripMenuItem1.Name = "RefreshtoolStripMenuItem1";
            this.RefreshtoolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.RefreshtoolStripMenuItem1.Size = new System.Drawing.Size(157, 26);
            this.RefreshtoolStripMenuItem1.Text = "Refresh";
            this.RefreshtoolStripMenuItem1.Click += new System.EventHandler(this.RefreshtoolStripMenuItem1_Click);
            // 
            // ucStudentAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ContextMenuStrip = this.refreshSearch;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucStudentAttendance";
            this.Size = new System.Drawing.Size(984, 632);
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtMarksAttendance)).EndInit();
            this.status.ResumeLayout(false);
            this.refreshSearch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbAttendanceId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddStudentAttendance;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip status;
        private System.Windows.Forms.ToolStripMenuItem presentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem absentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lateToolStripMenuItem;
        private System.Windows.Forms.DataGridView dtMarksAttendance;
        private System.Windows.Forms.ContextMenuStrip refreshSearch;
        private System.Windows.Forms.ToolStripMenuItem RefreshtoolStripMenuItem1;
    }
}

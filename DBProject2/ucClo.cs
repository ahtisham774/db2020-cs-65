﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucClo : UserControl
    {
        private static ucClo _instance;
        private String id = "";

        public static ucClo Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucClo();
                }
                return _instance;
            }
        }
        private ucClo()
        {
            InitializeComponent();
        }

        private void btnAddClo_Click(object sender, EventArgs e)
        {
            String name = txtCloName.Text;
            if(name != "")
            {
                var dateCreated = DateTime.Now.ToShortDateString();
                var dateUpdated = DateTime.Now.ToShortDateString();
                if(btnAddClo.Text == "Add")
                {
                    Insert(name, dateCreated, dateUpdated);
                }
                if(btnAddClo.Text == "Update")
                {
                    Update(name,dateUpdated,id);
                }
                dtViewClos.DataSource = Search("", "");
            }
            else
            {
                MessageBox.Show("Please Fill the field");
            }
        }
        private void Insert(String name,String cDate,String upDate)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Clo VALUES(@Name,@DateCreated,@DateUpdated)", con);
            cmd.Parameters.AddWithValue("@Name",name);
            cmd.Parameters.AddWithValue("@DateCreated", cDate);
            cmd.Parameters.AddWithValue("@DateUpdated", upDate);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        public DataTable Search(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd ;
            if(column != "")
            {
                cmd = new SqlCommand("SELECT * FROM Clo WHERE "+column+" = '"+val+"'", con);
               
            }
            else
            {
                cmd = new SqlCommand("SELECT * FROM Clo",con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private void Update(String name,String date, String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Clo SET Name = '"+name+"', DateUpdated = '"+date+"' WHERE id = '"+index+"'",con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Updated");
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if(cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if(txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewClos.DataSource = Search(column,value);
            }
        }

        private void viewAllClos_Click(object sender, EventArgs e)
        {
            dtViewClos.DataSource = Search("","");

        }

        private void dtViewClos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //String index;
            //try
            //{
            //    index = dtViewClos.Rows[e.RowIndex].Cells[0].Value.ToString();
            //    MessageBox.Show("Are you Sure to")
            //    MessageBox.Show(index);
            //}
            //catch(Exception ex) { }

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string str = txtSearch.Text;
            dtViewClos.DataSource = IsContains(str);
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Clo WHERE id LIKE '%"+str+"%' OR Name LIKE '%" + str + "%' OR DateCreated LIKE '%" + str + "%' OR DateUpdated LIKE'%" + str + "%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        private void Refresh(String option)
        {
            if (option == "Add")
            {
                txtCloName.Text = "";
                btnAddClo.Text = "Add";

            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }

        private void refreshCloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void refreshSearchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }

        private void dtViewClos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            String index = "";
            var dt = dtViewClos.Rows[0];
            try
            {
                dt = dtViewClos.Rows[e.RowIndex];
                index = dt.Cells[0].Value.ToString();
            }
            catch(Exception ex) { }
            if(index != "")
            {
                btnAddClo.Text = "Update";
                txtCloName.Text = dt.Cells[1].Value.ToString();
                id = index;
            }
        }
        public void getClos(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Clo", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getCloIndex(String clo)
        {
            String index = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM Clo WHERE Name = '" + clo + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            index = dt.Rows[0].ItemArray[0].ToString();
            return index;
        }
        public DataTable CloWiseResult()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT C.Name,COUNT(S.Id) AS [Number Of Students],(SELECT COUNT(*) FROM (SELECT DISTINCT StudentId FROM StudentResult) AS C) [Total Students],FLOOR((COUNT( S.Id)*1.0/(SELECT COUNT(*) FROM (SELECT DISTINCT StudentId FROM StudentResult) AS C))*100) AS [Class Result] FROM Student S INNER JOIN StudentResult SR ON S.Id = SR.StudentId INNER JOIN RubricLevel RL ON SR.RubricMeasurementId = RL.Id INNER JOIN Rubric R ON RL.RubricId = R.Id RIGHT OUTER JOIN Clo C ON C.Id = R.CloId GROUP BY C.Name;", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
    }
}

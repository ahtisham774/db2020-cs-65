﻿
namespace DBProject2
{
    partial class ucRubric
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucRubric));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.refreshAddRubric = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshSearch = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RefreshtoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewRubric = new System.Windows.Forms.DataGridView();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.view = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSearchValue = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDetail = new System.Windows.Forms.TextBox();
            this.btnAddRubric = new System.Windows.Forms.Button();
            this.cmbClo = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.refreshAddRubric.SuspendLayout();
            this.refreshSearch.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewRubric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // refreshAddRubric
            // 
            this.refreshAddRubric.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshAddToolStripMenuItem});
            this.refreshAddRubric.Name = "refreshAddRubric";
            this.refreshAddRubric.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshAddToolStripMenuItem
            // 
            this.refreshAddToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshAddToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshAddToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("refreshAddToolStripMenuItem.Image")));
            this.refreshAddToolStripMenuItem.Name = "refreshAddToolStripMenuItem";
            this.refreshAddToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshAddToolStripMenuItem.Size = new System.Drawing.Size(157, 26);
            this.refreshAddToolStripMenuItem.Text = "Refresh";
            this.refreshAddToolStripMenuItem.Click += new System.EventHandler(this.refreshAddToolStripMenuItem_Click);
            // 
            // refreshSearch
            // 
            this.refreshSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RefreshtoolStripMenuItem1});
            this.refreshSearch.Name = "refreshAddRubric";
            this.refreshSearch.Size = new System.Drawing.Size(158, 30);
            // 
            // RefreshtoolStripMenuItem1
            // 
            this.RefreshtoolStripMenuItem1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.RefreshtoolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RefreshtoolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("RefreshtoolStripMenuItem1.Image")));
            this.RefreshtoolStripMenuItem1.Name = "RefreshtoolStripMenuItem1";
            this.RefreshtoolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.RefreshtoolStripMenuItem1.Size = new System.Drawing.Size(157, 26);
            this.RefreshtoolStripMenuItem1.Text = "Refresh";
            this.RefreshtoolStripMenuItem1.Click += new System.EventHandler(this.RefreshtoolStripMenuItem1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtViewRubric);
            this.panel2.Location = new System.Drawing.Point(3, 364);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(981, 268);
            this.panel2.TabIndex = 1;
            // 
            // dtViewRubric
            // 
            this.dtViewRubric.AllowUserToAddRows = false;
            this.dtViewRubric.AllowUserToResizeColumns = false;
            this.dtViewRubric.AllowUserToResizeRows = false;
            this.dtViewRubric.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewRubric.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewRubric.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewRubric.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewRubric.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewRubric.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewRubric.ColumnHeadersHeight = 35;
            this.dtViewRubric.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewRubric.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewRubric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewRubric.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewRubric.Location = new System.Drawing.Point(0, 0);
            this.dtViewRubric.Name = "dtViewRubric";
            this.dtViewRubric.ReadOnly = true;
            this.dtViewRubric.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewRubric.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewRubric.RowHeadersVisible = false;
            this.dtViewRubric.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewRubric.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtViewRubric.Size = new System.Drawing.Size(981, 268);
            this.dtViewRubric.TabIndex = 17;
            this.dtViewRubric.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtViewRubric_CellClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSearch.Location = new System.Drawing.Point(27, 304);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(359, 29);
            this.txtSearch.TabIndex = 14;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // view
            // 
            this.view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.view.AutoSize = true;
            this.view.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Location = new System.Drawing.Point(879, 314);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(66, 21);
            this.view.TabIndex = 15;
            this.view.Text = "View All";
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(25, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(929, 256);
            this.splitContainer1.SplitterDistance = 500;
            this.splitContainer1.TabIndex = 16;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.refreshSearch;
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(425, 256);
            this.panel4.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "Details",
            "Cloid"});
            this.cmbColumn.Location = new System.Drawing.Point(138, 49);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(247, 28);
            this.cmbColumn.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtSearchValue.Location = new System.Drawing.Point(138, 100);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(247, 27);
            this.txtSearchValue.TabIndex = 9;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(27, 165);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(358, 37);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ContextMenuStrip = this.refreshAddRubric;
            this.panel3.Controls.Add(this.cmbClo);
            this.panel3.Controls.Add(this.btnAddRubric);
            this.panel3.Controls.Add(this.txtDetail);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(500, 256);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(28, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Detail:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "CLO:";
            // 
            // txtDetail
            // 
            this.txtDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDetail.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetail.Location = new System.Drawing.Point(126, 44);
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.Size = new System.Drawing.Size(332, 27);
            this.txtDetail.TabIndex = 1;
            // 
            // btnAddRubric
            // 
            this.btnAddRubric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddRubric.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddRubric.Location = new System.Drawing.Point(36, 172);
            this.btnAddRubric.Name = "btnAddRubric";
            this.btnAddRubric.Size = new System.Drawing.Size(422, 32);
            this.btnAddRubric.TabIndex = 7;
            this.btnAddRubric.Text = "Add";
            this.btnAddRubric.UseVisualStyleBackColor = true;
            this.btnAddRubric.Click += new System.EventHandler(this.btnAddRubric_Click);
            // 
            // cmbClo
            // 
            this.cmbClo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbClo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbClo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbClo.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbClo.FormattingEnabled = true;
            this.cmbClo.Location = new System.Drawing.Point(126, 109);
            this.cmbClo.Name = "cmbClo";
            this.cmbClo.Size = new System.Drawing.Size(332, 28);
            this.cmbClo.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.view);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 355);
            this.panel1.TabIndex = 0;
            // 
            // ucRubric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucRubric";
            this.Size = new System.Drawing.Size(984, 632);
            this.refreshAddRubric.ResumeLayout(false);
            this.refreshSearch.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewRubric)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dtViewRubric;
        private System.Windows.Forms.ContextMenuStrip refreshAddRubric;
        private System.Windows.Forms.ToolStripMenuItem refreshAddToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip refreshSearch;
        private System.Windows.Forms.ToolStripMenuItem RefreshtoolStripMenuItem1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label view;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbClo;
        private System.Windows.Forms.Button btnAddRubric;
        private System.Windows.Forms.TextBox txtDetail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
    }
}

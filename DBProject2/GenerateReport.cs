﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBProject2
{
    public class GenerateReport
    {
        public GenerateReport()
        {
            
        }

        public static void generateCLOWiseReport(String filepath,String Title)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();

            PdfWriter writer = PdfWriter.GetInstance(document, file);
            document.Open();
            document.AddAuthor("Ahtisham Ul Haq");

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font contentfont = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font headingfont = new iTextSharp.text.Font(bf, 20, iTextSharp.text.Font.BOLD);

            Image img = Image.GetInstance(@"E:\data\Images\uetLogo.png");
            img.Alignment = Element.ALIGN_LEFT;
            img.ScaleToFit(50,50);
            document.Add(img);

            Paragraph title = new Paragraph(Title+"\n", headingfont);
            title.Alignment = Element.ALIGN_CENTER;
            document.Add(title);
            document.Add(new Paragraph("\n", font));

            LineSeparator line = new LineSeparator(1f, 100f,iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);

            String str = "This Table shows Percentage of Students Whose attends the following Clos.";
            Paragraph content = new Paragraph(str,contentfont);
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            PdfPTable table = new PdfPTable(4);
            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 20;
            table.DefaultCell.Padding = 2;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell caption = new PdfPCell(new Phrase("Table1. Students who attend clos"));
            caption.Colspan = 4;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            Paragraph heading = new Paragraph("CLO",font);
            table.AddCell(heading);
            heading = new Paragraph("No. of Student", font);
            table.AddCell(heading);
            heading = new Paragraph("Total Students", font);
            table.AddCell(heading); 
            heading = new Paragraph("Class Result", font);
            table.AddCell(heading);
            table.DefaultCell.BackgroundColor = null;

            foreach (DataRow row in ucClo.Instance.CloWiseResult().Rows)
            {
                Paragraph cell = new Paragraph(row.ItemArray[0].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[1].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[2].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[3].ToString() + "%", contentfont);
                table.AddCell(cell);
            }

            document.Add(table);
            document.Close();
        }
        public static void generateAssessmentWiseReport(String filepath, String Title)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, file);

            document.Open();
            document.AddAuthor("Ahtisham Ul Haq");

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font contentfont = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font headingfont = new iTextSharp.text.Font(bf, 20, iTextSharp.text.Font.BOLD);

            Image img = Image.GetInstance(@"E:\data\Images\uetLogo.png");
            img.Alignment = Element.ALIGN_LEFT;
            img.ScaleToFit(50, 50);
            document.Add(img);

            Paragraph title = new Paragraph(Title + "\n", headingfont);
            title.Alignment = Element.ALIGN_CENTER;
            document.Add(title);
            document.Add(new Paragraph("\n", font));

            LineSeparator line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);

            String str = "This Table shows Percentage of Students Whose attends the following Assessments.";
            Paragraph content = new Paragraph(str, contentfont);
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            PdfPTable table = new PdfPTable(4);
            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 20;
            table.DefaultCell.Padding = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell caption = new PdfPCell(new Phrase("Table1. Students who attend assessments"));
            caption.Colspan = 4;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            Paragraph heading = new Paragraph("Title", font);
            table.AddCell(heading);
            heading = new Paragraph("No. of Student", font);
            table.AddCell(heading);
            heading = new Paragraph("Total Students", font);
            table.AddCell(heading);
            heading = new Paragraph("Class Result", font);
            table.AddCell(heading);
            table.DefaultCell.BackgroundColor = null;

            foreach (DataRow row in ucAssessment.Instance.AssessmentWiseResult().Rows)
            {
                Paragraph cell = new Paragraph(row.ItemArray[0].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[1].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[2].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[3].ToString() + "%", contentfont);
                table.AddCell(cell);
            }

            document.Add(table);
            document.Close();
        }
        public static void generateStudentAttendAssessment(String filepath, String Title,String type)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, file);

            document.Open();
            document.AddAuthor("Ahtisham Ul Haq");

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font contentfont = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font headingfont = new iTextSharp.text.Font(bf, 20, iTextSharp.text.Font.BOLD);

            Image img = Image.GetInstance(@"E:\data\Images\uetLogo.png");
            img.Alignment = Element.ALIGN_LEFT;
            img.ScaleToFit(50, 50);
            document.Add(img);

            Paragraph title = new Paragraph(Title + "\n", headingfont);
            title.Alignment = Element.ALIGN_CENTER;
            document.Add(title);
            document.Add(new Paragraph("\n", font));

            LineSeparator line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);

            String str = "This Table shows  Students Whose attends "+type;
            Paragraph content = new Paragraph(str, contentfont);
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            PdfPTable table = new PdfPTable(2);
            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 20;
            table.DefaultCell.Padding = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell caption = new PdfPCell(new Phrase("Table1.Students attend " + type));
            caption.Colspan = 2;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            Paragraph heading = new Paragraph("Student", font);
            table.AddCell(heading);

            heading = new Paragraph("Result", font);
            table.AddCell(heading);
            table.DefaultCell.BackgroundColor = null;

            foreach (DataRow row in ucStudentResult.Instance.StudentsWhoAttends(type).Rows)
            {
                Paragraph cell = new Paragraph(row.ItemArray[0].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph(row.ItemArray[1].ToString(), contentfont);
                table.AddCell(cell);
            }

            table.SpacingAfter = 30;
            document.Add(table);

            /// Table 2
            line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);

            str = "This Table shows  Students Whose Not attends " + type;
            content = new Paragraph(str, contentfont);
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            table = new PdfPTable(2);
            table.SpacingBefore = 30;
            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 20;
            table.DefaultCell.Padding = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            caption = new PdfPCell(new Phrase("Table2.Students not attend " + type));
            caption.Colspan = 2;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            heading = new Paragraph("Student", font);
            table.AddCell(heading);

            heading = new Paragraph("Result", font);
            table.AddCell(heading);
            table.DefaultCell.BackgroundColor = null;

            foreach (DataRow row in ucStudentResult.Instance.StudentsWhoNotAttends(type).Rows)
            {
                Paragraph cell = new Paragraph(row.ItemArray[0].ToString(), contentfont);
                table.AddCell(cell);
                cell = new Paragraph("Not Attend", contentfont);
                table.AddCell(cell);
            }

            document.Add(table);
            document.Close();
        }

        public static void RubricsReport(String filepath, String Title)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, file);

            document.Open();
            document.AddAuthor("Ahtisham Ul Haq");

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font contentfont = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font headingfont = new iTextSharp.text.Font(bf, 20, iTextSharp.text.Font.BOLD);

            Image img = Image.GetInstance(@"E:\data\Images\uetLogo.png");
            img.Alignment = Element.ALIGN_LEFT;
            img.ScaleToFit(50, 50);
            document.Add(img);

            Paragraph title = new Paragraph(new Phrase(Title + "\n", headingfont));
            title.Alignment = Element.ALIGN_CENTER;
            document.Add(title);
            document.Add(new Paragraph("\n", font));

            LineSeparator line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);
            String str = "This Table shows Rubrics Of Each Assessments.";
            Paragraph content = new Paragraph(new Phrase(str, contentfont));
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            var dt = ucRubric.Instance.Rubrics();

            PdfPTable table = new PdfPTable(dt.Columns.Count);
            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 35;
            table.WidthPercentage = 100;
            table.TotalWidth = 510;
            var width = new[] { 20f, 28f, 19f, 25f, 25f, 25f, 10f };
            table.SetWidths(width);
            table.LockedWidth = true;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.PaddingTop = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell caption = new PdfPCell(new Phrase("Table1. Rubrics of each assessments"));
            caption.Colspan = dt.Columns.Count;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            foreach (DataColumn col in dt.Columns)
            {
                Paragraph heading = new Paragraph(col.ColumnName, font);
                table.AddCell(heading);
            }

            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.DefaultCell.BackgroundColor = null;
            table.DefaultCell.FixedHeight = 25;
            table.DefaultCell.PaddingTop = 1;

            foreach (DataRow row in dt.Rows)
            {
                foreach(var item in row.ItemArray)
                {
                    Paragraph cell = new Paragraph(item.ToString(), contentfont);
                    table.AddCell(cell);
                }
            }

            document.Add(table);
            document.Close();
        }

        public static void AttendaceReport(String filepath, String Title)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, file);

            document.Open();
            document.AddAuthor("Ahtisham Ul Haq");

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font contentfont = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font headingfont = new iTextSharp.text.Font(bf, 20, iTextSharp.text.Font.BOLD);

            Image img = Image.GetInstance(@"E:\data\Images\uetLogo.png");
            img.Alignment = Element.ALIGN_LEFT;
            img.ScaleToFit(50, 50);
            document.Add(img);

            //  DAte wise attendance display
            Paragraph title = new Paragraph(new Phrase(Title + "\n", headingfont));
            title.Alignment = Element.ALIGN_CENTER;
            document.Add(title);
            document.Add(new Paragraph("\n", font));

            LineSeparator line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);
           
            String str = "This Table shows Date Wise Attendance.";
            Paragraph content = new Paragraph(new Phrase(str, contentfont));
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            var dt = ucViewAttendance.Instance.DailyAttendancePercentage();

            PdfPTable table = new PdfPTable(dt.Columns.Count);

            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 35;
            table.WidthPercentage = 100;
            table.TotalWidth = 510;
            table.LockedWidth = true;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.PaddingTop = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell caption = new PdfPCell(new Phrase("Table1. Date wise attendance"));
            caption.Colspan = dt.Columns.Count;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            foreach (DataColumn col in dt.Columns)
            {
                Paragraph heading = new Paragraph(col.ColumnName, font);
                table.AddCell(heading);
            }

            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.DefaultCell.BackgroundColor = null;
            table.DefaultCell.FixedHeight = 25;
            table.DefaultCell.PaddingTop = 1;

            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    Paragraph cell = new Paragraph(item.ToString(), contentfont);
                    table.AddCell(cell);
                }
            }

            table.SpacingAfter = 30;
            document.Add(table);



            //  Month wise attendance display
            line = new LineSeparator(1f, 100f, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1);
            document.Add(line);
            str = "This Table shows Month Wise Attendance.";
            content = new Paragraph(new Phrase(str, contentfont));
            content.SpacingAfter = 20;
            content.SpacingBefore = 20;
            document.Add(content);

            dt = ucViewAttendance.Instance.MonthWisePercentage();

            table = new PdfPTable(dt.Columns.Count);

            table.SpacingBefore = 20;
            table.DefaultCell.FixedHeight = 35;
            table.WidthPercentage = 100;
            table.TotalWidth = 510;
            table.LockedWidth = true;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.PaddingTop = 4;
            table.DefaultCell.BackgroundColor = BaseColor.LIGHT_GRAY;

            caption = new PdfPCell(new Phrase("Table1. Month wise attendance"));
            caption.Colspan = dt.Columns.Count;
            caption.Border = 0;
            caption.HorizontalAlignment = 1;
            table.AddCell(caption);

            foreach (DataColumn col in dt.Columns)
            {
                Paragraph heading = new Paragraph(col.ColumnName, font);
                table.AddCell(heading);
            }

            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
            table.DefaultCell.BackgroundColor = null;
            table.DefaultCell.FixedHeight = 25;
            table.DefaultCell.PaddingTop = 1;

            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    Paragraph cell = new Paragraph(item.ToString(), contentfont);
                    table.AddCell(cell);
                }
            }

            document.Add(table);
            document.Close();
        }
    }

}

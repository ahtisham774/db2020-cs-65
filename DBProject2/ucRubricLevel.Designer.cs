﻿
namespace DBProject2
{
    partial class ucRubricLevel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucRubricLevel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.view = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.addRubricLevelRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshAddRubricLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.numMeasurementLevel = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbRubric = new System.Windows.Forms.ComboBox();
            this.btnAddRubricLevel = new System.Windows.Forms.Button();
            this.txtDetail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SearchRubricLevelRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshSearchRubricLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewRubricLevel = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.addRubricLevelRefresh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMeasurementLevel)).BeginInit();
            this.panel4.SuspendLayout();
            this.SearchRubricLevelRefresh.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewRubricLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.view);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(0, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(981, 351);
            this.panel1.TabIndex = 0;
            // 
            // view
            // 
            this.view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.view.AutoSize = true;
            this.view.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Location = new System.Drawing.Point(889, 312);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(66, 21);
            this.view.TabIndex = 9;
            this.view.Text = "View All";
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSearch.Location = new System.Drawing.Point(25, 304);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(359, 29);
            this.txtSearch.TabIndex = 8;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(26, 21);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(929, 256);
            this.splitContainer1.SplitterDistance = 469;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ContextMenuStrip = this.addRubricLevelRefresh;
            this.panel3.Controls.Add(this.numMeasurementLevel);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cmbRubric);
            this.panel3.Controls.Add(this.btnAddRubricLevel);
            this.panel3.Controls.Add(this.txtDetail);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(469, 256);
            this.panel3.TabIndex = 2;
            // 
            // addRubricLevelRefresh
            // 
            this.addRubricLevelRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshAddRubricLevel});
            this.addRubricLevelRefresh.Name = "addRubricLevelRefresh";
            this.addRubricLevelRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshAddRubricLevel
            // 
            this.refreshAddRubricLevel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshAddRubricLevel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshAddRubricLevel.Image = ((System.Drawing.Image)(resources.GetObject("refreshAddRubricLevel.Image")));
            this.refreshAddRubricLevel.Name = "refreshAddRubricLevel";
            this.refreshAddRubricLevel.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshAddRubricLevel.Size = new System.Drawing.Size(157, 26);
            this.refreshAddRubricLevel.Text = "Refresh";
            this.refreshAddRubricLevel.Click += new System.EventHandler(this.refreshAddRubricLevel_Click);
            // 
            // numMeasurementLevel
            // 
            this.numMeasurementLevel.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.numMeasurementLevel.Location = new System.Drawing.Point(181, 152);
            this.numMeasurementLevel.Name = "numMeasurementLevel";
            this.numMeasurementLevel.Size = new System.Drawing.Size(253, 27);
            this.numMeasurementLevel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Measurement Level:";
            // 
            // cmbRubric
            // 
            this.cmbRubric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbRubric.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbRubric.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbRubric.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbRubric.FormattingEnabled = true;
            this.cmbRubric.Location = new System.Drawing.Point(127, 86);
            this.cmbRubric.Name = "cmbRubric";
            this.cmbRubric.Size = new System.Drawing.Size(307, 28);
            this.cmbRubric.TabIndex = 2;
            // 
            // btnAddRubricLevel
            // 
            this.btnAddRubricLevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddRubricLevel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddRubricLevel.Location = new System.Drawing.Point(23, 198);
            this.btnAddRubricLevel.Name = "btnAddRubricLevel";
            this.btnAddRubricLevel.Size = new System.Drawing.Size(408, 38);
            this.btnAddRubricLevel.TabIndex = 4;
            this.btnAddRubricLevel.Text = "Add";
            this.btnAddRubricLevel.UseVisualStyleBackColor = true;
            this.btnAddRubricLevel.Click += new System.EventHandler(this.btnAddRubricLevel_Click);
            // 
            // txtDetail
            // 
            this.txtDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDetail.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetail.Location = new System.Drawing.Point(128, 21);
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.Size = new System.Drawing.Size(307, 27);
            this.txtDetail.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Rubric ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Detail:";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.SearchRubricLevelRefresh;
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(2, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(451, 253);
            this.panel4.TabIndex = 3;
            // 
            // SearchRubricLevelRefresh
            // 
            this.SearchRubricLevelRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshSearchRubricLevel});
            this.SearchRubricLevelRefresh.Name = "addRubricLevelRefresh";
            this.SearchRubricLevelRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshSearchRubricLevel
            // 
            this.refreshSearchRubricLevel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshSearchRubricLevel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshSearchRubricLevel.Image = ((System.Drawing.Image)(resources.GetObject("refreshSearchRubricLevel.Image")));
            this.refreshSearchRubricLevel.Name = "refreshSearchRubricLevel";
            this.refreshSearchRubricLevel.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshSearchRubricLevel.Size = new System.Drawing.Size(157, 26);
            this.refreshSearchRubricLevel.Text = "Refresh";
            this.refreshSearchRubricLevel.Click += new System.EventHandler(this.refreshSearchRubricLevel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(24, 188);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(404, 39);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtSearchValue.Location = new System.Drawing.Point(146, 119);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(279, 27);
            this.txtSearchValue.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "Details",
            "RubricId",
            "MeasurementLevel"});
            this.cmbColumn.Location = new System.Drawing.Point(146, 37);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(279, 28);
            this.cmbColumn.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtViewRubricLevel);
            this.panel2.Location = new System.Drawing.Point(0, 354);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(981, 276);
            this.panel2.TabIndex = 1;
            // 
            // dtViewRubricLevel
            // 
            this.dtViewRubricLevel.AllowUserToAddRows = false;
            this.dtViewRubricLevel.AllowUserToResizeColumns = false;
            this.dtViewRubricLevel.AllowUserToResizeRows = false;
            this.dtViewRubricLevel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewRubricLevel.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewRubricLevel.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewRubricLevel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewRubricLevel.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewRubricLevel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewRubricLevel.ColumnHeadersHeight = 35;
            this.dtViewRubricLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewRubricLevel.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewRubricLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewRubricLevel.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewRubricLevel.Location = new System.Drawing.Point(0, 0);
            this.dtViewRubricLevel.Name = "dtViewRubricLevel";
            this.dtViewRubricLevel.ReadOnly = true;
            this.dtViewRubricLevel.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewRubricLevel.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewRubricLevel.RowHeadersVisible = false;
            this.dtViewRubricLevel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewRubricLevel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtViewRubricLevel.Size = new System.Drawing.Size(981, 276);
            this.dtViewRubricLevel.TabIndex = 10;
            this.dtViewRubricLevel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtViewRubricLevel_CellClick);
            // 
            // ucRubricLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucRubricLevel";
            this.Size = new System.Drawing.Size(984, 632);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.addRubricLevelRefresh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numMeasurementLevel)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.SearchRubricLevelRefresh.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewRubricLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbRubric;
        private System.Windows.Forms.Button btnAddRubricLevel;
        private System.Windows.Forms.TextBox txtDetail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label view;
        private System.Windows.Forms.DataGridView dtViewRubricLevel;
        private System.Windows.Forms.NumericUpDown numMeasurementLevel;
        private System.Windows.Forms.ContextMenuStrip addRubricLevelRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshAddRubricLevel;
        private System.Windows.Forms.ContextMenuStrip SearchRubricLevelRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshSearchRubricLevel;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucStudent : UserControl
    {
       
        private static ucStudent _instence; // used for singleton class

        /* These attributes are used to help for update function */

        private String checkEmail = "";
        private String checkContact = "";
        private String index = "";
        private String checkFname = "";
        private String checkLname = "";
        private String checkStatus = "";
        public static ucStudent Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucStudent();
                }
                return _instence;
            }
        }
        private ucStudent()
        {
            InitializeComponent();
           
        }
        /* This function get registration number from mask box 
         * and if there were sapce in registration number than remove*/
        private String getRegistration(String regNo)
        {
            String str = "";
            foreach (char c in regNo)
            {
                if (c == ' ')
                {

                }
                else
                {
                    str += c;
                }

            }
            if(str.Length == 2)
            {
                return "";
            }
            return str;
        }
        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            bool isEmailFound = false;
            bool isContactFound = false;
            bool isRegNoFound = false;
            String firstName = txtFirstName.Text;
            String lastName = txtLastName.Text;
            String contact = getContact(txtContact.Text);
            String email = txtEmail.Text;
            String regNo = getRegistration(txtRegNo.Text);
            int status = 6;
            if (cbStatus.Checked)
            {
                status = 5;
            }
            if(firstName == "" || email == "" || regNo == "")
            {
                MessageBox.Show("Please Fill The Fields");
            }
            else
            {
                if(btnAddStudent.Text == "Add")
                {
                    if (isFound("RegistrationNumber", regNo) && regNo != "")
                    {
                        isRegNoFound = true;
                        MessageBox.Show(regNo + " Already Exist");
                    }
                    if (isFound("Email", email) && email != "")
                    {
                        isEmailFound = true;
                        MessageBox.Show(email + " Already Exist");
                    }
                    if (isFound("Contact", contact) && contact != "")
                    {
                        isContactFound = true;
                        MessageBox.Show(contact + " Already Exist");
                    }
                    if (isEmailFound == false && isRegNoFound == false && isContactFound == false)
                    {


                        Insert(firstName, lastName, contact, email, regNo, status, getReseed());
                        dtViewStudent.DataSource = Get("","");
                    }
                }
                if(btnAddStudent.Text == "Update")
                {
                    isEmailFound = false;
                    isContactFound = false;
                    if (email != "")
                    {
                        if (!isEmailFoundForUpdate(email))
                        {
                            isEmailFound = true;
                            MessageBox.Show("Email Already Exists");
                        }
                    }
                    if (contact != "")
                    {
                        if (!isContactFoundForUpdate(contact))
                        {
                            isContactFound = true;
                            MessageBox.Show("Contact Already Exists");
                        }
                    }

                    if (isEmailFound == false && isContactFound == false)
                    {
                        
                        Update("FirstName", firstName, index);
                        Update("LastName", lastName, index);
                        Update("Email", email, index);
                        Update("Contact", contact, index);
                        Update("Status", status.ToString(), index);
                        MessageBox.Show("Data is Updated");
                        dtViewStudent.DataSource = Get("", "");
                        
                    }
                }
            }
            
        }

        private String getContact(String contct)
        {
            String str = "";
            foreach(char c in contct)
            {
                if(c == ' ' || c == '-' || c == '(' || c==')'){

                }
                else
                {
                    str += c;
                }
                
            }
            return str;
        }
        private void Insert(String v1,String v2,String v3,String v4,String v5,int v6,String reseed)
        {
          
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Student VALUES(@v1, @v2, @v3, @v4, @v5,@v6)", con);
            cmd.Parameters.AddWithValue("@v1", v1);
            if(v2 == "")
            {
                cmd.Parameters.AddWithValue("@v2", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@v2", v2);
            }
            if (v3 == "")
            {
                cmd.Parameters.AddWithValue("@v3", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@v3", v3);
            }
            cmd.Parameters.AddWithValue("@v4", v4);
            cmd.Parameters.AddWithValue("@v5", v5);
            cmd.Parameters.AddWithValue("@v6", v6);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data in inserted");
            
            
        }

        private DataTable Get(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if (val != "")
            {
                cmd = new SqlCommand("SELECT id,FirstName,LastName,Contact,Email,RegistrationNumber, Status = (SELECT Name FROM Lookup WHERE Status = LookupId) FROM Student  WHERE " + column + " = @val", con);
                cmd.Parameters.AddWithValue("@val", val);
            }
            else
            {
                cmd = new SqlCommand("SELECT id,FirstName,LastName,Contact,Email,RegistrationNumber, Status = (SELECT Name FROM Lookup WHERE Status = LookupId) FROM Student ", con);
            }

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable tb = new DataTable();
            adpt.Fill(tb);
            return tb;
        }
        private bool isFound(String col, String val)
        {
            DataTable dt = Get(col,val);
            if(dt.Rows.Count >= 1)
            {
                return true;
            }
            return false;
        }

        private void view_Click(object sender, EventArgs e)
        {
          
            dtViewStudent.DataSource = Get("","");
            
        }
        private String getReseed()
        {
            DataTable dt = Get("", "");
            String s = dt.Rows[dt.Rows.Count - 1].ItemArray[0].ToString();
            return s;
        }

        private void rbActive_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dt = Get("Status", "5");
            dtViewStudent.DataSource = dt;
            rbActive.Checked = false;
        }

        private void rbInActive_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dt = Get("Status", "6");
            dtViewStudent.DataSource = dt;
            rbInActive.Checked = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            if (cmbColumn.SelectedIndex != -1)
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            String val = txtSearchData.Text;
            if(column == "")
            {
                MessageBox.Show("Please Select Column First");
            }
            if(val == "")
            {
                MessageBox.Show("Please Enter value to search of desired columns");
            }
            if(!(column=="") && !(val == ""))
            {
                DataTable dt = Get(column, val);
                if(dt.Rows.Count == 0)
                {
                    MessageBox.Show("No Records Found Of " + val);
                }
                else
                {
                    dtViewStudent.DataSource = dt;
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            dtViewStudent.DataSource = IsContains(txtSearch.Text);
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id,FirstName,LastName,Contact,Email,RegistrationNumber, Status = (SELECT Name FROM Lookup WHERE Status = LookupId) FROM Student  WHERE id LIKE '%" + str+"%' OR FirstName+' '+LastName LIKE '%" + str + "%' OR LastName LIKE '%"+str+"%' OR Contact LIKE '%"+str+"%' OR RegistrationNumber LIKE'%"+str+"%' OR Email LIKE '%"+str+"%' OR Status LIKE'%"+str+"%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }

        private void dtViewStudent_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            String count = "";
            var dtTable = dtViewStudent.Rows[0];
            try
            {
                dtTable = dtViewStudent.Rows[e.RowIndex];
                count = dtTable.Cells[0].Value.ToString();

            }
            catch (Exception ex)
            {

            }
            if (count != "")
            {
                txtRegNo.ReadOnly = true;

                /* Check variables are used in  update Section to check whether 
                 * data is changed or not */
                checkFname = dtTable.Cells[1].Value.ToString();
                txtFirstName.Text = checkFname;

                checkLname = dtTable.Cells[2].Value.ToString();
                txtLastName.Text = checkLname;

                txtRegNo.Text = getUpdateRegNo(dtTable.Cells[5].Value.ToString());

                checkEmail = dtTable.Cells[4].Value.ToString();
                txtEmail.Text = checkEmail;

                checkContact = dtTable.Cells[3].Value.ToString();
                txtContact.Text = checkContact;

                checkStatus = dtTable.Cells[6].Value.ToString();
                if (checkStatus == "Active")
                {
                    cbStatus.Checked = true;
                }
                else
                {
                    cbStatus.Checked = false;
                }
                btnAddStudent.Text = "Update";
                /* This variable is used to get index of record */
                index = count;
            }


        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Update");
        }

        private String getUpdateRegNo(String regNo)
        {
            String []s = regNo.Split('-');
            if(s[1].Length == 2)
            {
                s[1] +=' ' ;
            }
            return s[0]+s[1]+s[2];
        }
        private void refresh(String option)
        {
            if(option == "Add")
            {
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtRegNo.Text = "";
                txtContact.Text = "";
                txtEmail.Text = "";
                cbStatus.Checked = false;
                btnAddStudent.Text = "Add";
                txtRegNo.ReadOnly = false;
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchData.Text = "";
            }
        }

        private void refreshaddStd_Click(object sender, EventArgs e)
        {
            refresh("Add");
        }

        private void refreshSearchStd_Click(object sender, EventArgs e)
        {
            refresh("Search");
        }
        private bool isEmailFoundForUpdate(String email)
        {
            DataTable dt;
            dt = Get("Email", email);
            if(checkEmail != "")
            {
                if (dt.Rows.Count >= 1 && email != checkEmail)
                {
                    return false;
                }
            }
            else
            {
                if (dt.Rows.Count >= 1)
                {
                    return false;
                }
            }
            return true;
        }
        private bool isContactFoundForUpdate(String contact)
        {
            DataTable dt;
            dt = Get("Contact", contact);
            if(checkContact != "")
            {
                if (dt.Rows.Count >= 1 && contact != checkContact)
                {
                    return false;
                }
            }
            else
            {
                if (dt.Rows.Count >= 1)
                {
                    return false;
                }
            }
          
            return true;
        }
        private void search_TextChanged(object sender, EventArgs e)
        {
            dtViewStudent.DataSource = IsContains(search.Text);
        }
        private void Update(String column, String val, String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if (val != "")
            {
                cmd = new SqlCommand("UPDATE Student SET " + column + " = '" + val + "' WHERE id = '" + index + "'", con);
            }
            else
            {
                cmd = new SqlCommand("UPDATE Student SET "+column+"=  @val WHERE id = @index", con);
                cmd.Parameters.AddWithValue("@val", DBNull.Value);
                cmd.Parameters.AddWithValue("@index", index);
            } 
            cmd.ExecuteNonQuery();
          
        }
        private DataGridViewCellEventArgs location;
        private void deleteStudent_Click(object sender, EventArgs e)
        {
            String index = "";
            try
            {
                index = dtViewStudent.Rows[location.RowIndex].Cells[0].Value.ToString();
                DialogResult result =  MessageBox.Show("Are you sure to delete the data of id = " + index+ "\nIf you do so then this data also remove from StuentResult and other relevent Tables.","Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) ;
                if(result == DialogResult.Yes)
                {
                    MessageBox.Show(index);
                    Delete(index);
                }
                
            }
            catch(Exception ex) { }
        }

        private void dtViewStudent_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            location = e;
        }
        private void Delete(String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE Student WHERE id = '" + index +"'", con) ;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data Deleted");
        }
        public void getStudentRego(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT RegistrationNumber FROM Student", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getStudentIndex(String regNo)
        {
            String index = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM Student WHERE RegistrationNumber = '" + regNo + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            index = dt.Rows[0].ItemArray[0].ToString();
            return index;
        }
        public String getStudentRegNoValue(String index)
        {
           
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT RegistrationNumber FROM Student WHERE id  = " + index, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
            
        }
        public DataTable getAllStudentRegNo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT RegistrationNumber FROM Student ORDER BY RegistrationNumber", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
    }
}

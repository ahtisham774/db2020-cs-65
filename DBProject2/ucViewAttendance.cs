﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucViewAttendance : UserControl
    {
        private static ucViewAttendance _instance;
        public static ucViewAttendance Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucViewAttendance();
                }
                return _instance;
            }
        }
        private ucViewAttendance()
        {
            InitializeComponent();
        }
        public DataTable Search(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if (column != "" && val != "")
            {
                cmd = new SqlCommand("SELECT * FROM StudentAttendance WHERE " + column + "= '" + val + "'", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT * FROM StudentAttendance", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }

        private void view_Click(object sender, EventArgs e)
        {
            DataTable dt = Search("", "");
            getValuesOfIndicesOfStudentAttendance(dt);
        }
        private void getValuesOfIndicesOfStudentAttendance(DataTable indices)
        {
            
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Date = (SELECT AttendanceDate FROM ClassAttendance WHERE ST.AttendanceId = id),Student = (SELECT RegistrationNumber FROM Student WHERE ST.StudentId = id), Status = (SELECT Name FROM Lookup WHERE ST.AttendanceStatus = LookupId) FROM StudentAttendance ST ORDER BY Date,Student", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
          
            dtViewStudentAttendance.DataSource = dt;
        }
        private DataTable StatusFilter()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Status= (SELECT Name FROM Lookup WHERE ST.AttendanceStatus = LookupId),COUNT(*) AS Count FROM StudentAttendance ST GROUP BY ST.AttendanceStatus", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private DataTable StudentRegNoFilter()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Students= (SELECT RegistrationNumber FROM Student WHERE ST.StudentId = id),Status= (SELECT Name FROM Lookup WHERE ST.AttendanceStatus = LookupId),COUNT(*) AS Count FROM StudentAttendance ST GROUP BY ST.StudentId,St.AttendanceStatus ORDER BY Students", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private DataTable DateFilter()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Date= (SELECT AttendanceDate FROM ClassAttendance WHERE ST.AttendanceId = id),Status= (SELECT Name FROM Lookup WHERE ST.AttendanceStatus = LookupId),COUNT(*) AS Count FROM StudentAttendance ST GROUP BY ST.AttendanceId,ST.AttendanceStatus ORDER BY date", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private DataTable MonthWiseFilter()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FORMAT(CL.AttendanceDate,'MMMM') AS Month,look.Name,COUNT(*) AS Count FROM ClassAttendance CL INNER JOIN StudentAttendance ST ON CL.Id = ST.AttendanceId INNER JOIN Lookup look ON ST.AttendanceStatus = look.LookupId INNER JOIN Student S ON ST.StudentId = S.Id GROUP BY FORMAT(CL.AttendanceDate,'MMMM'),look.Name ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private DataTable MonthWiseEachStudentFilter()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FORMAT(CL.AttendanceDate,'MMMM') AS MONTH,S.RegistrationNumber,look.Name,COUNT(*) AS COUNT FROM ClassAttendance CL INNER JOIN StudentAttendance ST ON CL.Id = ST.AttendanceId INNER JOIN Lookup look ON ST.AttendanceStatus = look.LookupId INNER JOIN Student S ON ST.StudentId = S.Id GROUP BY FORMAT(CL.AttendanceDate,'MMMM'),S.RegistrationNumber,look.Name ORDER BY S.RegistrationNumber ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        public DataTable MonthWisePercentage()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FORMAT(attendance.Date,'MMMM') AS MONTH,CEILING(AVG(attendance.Present)) AS Present,CEILING(AVG(attendance.Absent)) AS Absent,FLOOR(AVG(attendance.Leave)) AS Leave,FLOOR(AVG(attendance.Late)) AS Late FROM (SELECT DISTINCT Date= (SELECT AttendanceDate FROM ClassAttendance WHERE Id = ST.AttendanceId),Present= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 1)*1.0/(SELECT  COUNT(*) FROM Student)*100),Absent= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 2)*1.0/(SELECT  COUNT(*) FROM Student)*100),Leave= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 3)*1.0/(SELECT  COUNT(*) FROM Student)*100),Late= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 4)*1.0/(SELECT  COUNT(*) FROM Student)*100) FROM StudentAttendance ST) AS attendance GROUP BY FORMAT(attendance.date,'MMMM')", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        public DataTable DailyAttendancePercentage()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT Date= FORMAT((SELECT AttendanceDate FROM ClassAttendance WHERE Id = ST.AttendanceId),'dd-MMMM-yyyy'),Present= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 1)*1.0/(SELECT  COUNT(*) FROM Student)*100),Absent= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 2)*1.0/(SELECT  COUNT(*) FROM Student)*100),Leave= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 3)*1.0/(SELECT  COUNT(*) FROM Student)*100),Late= FLOOR((SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = St.AttendanceId AND AttendanceStatus = 4)*1.0/(SELECT  COUNT(*) FROM Student)*100) FROM StudentAttendance ST", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        //private void cbDate_CheckedChanged(object sender, EventArgs e)
        //{

        //}

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Value == 0)
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Value.ToString();

                getValuesOfIndicesOfStudentAttendance(Search(column, value));
                
            }
        }
        private void Refresh(String option)
        {
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }

        private void RefreshtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }

        //private void radioButton3_CheckedChanged(object sender, EventArgs e)
        //{

        //}

        private void rbStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (rbStatus.Checked)
            {

                dtViewStudentAttendance.DataSource = StatusFilter();
            }
        }

        private void rbStdId_CheckedChanged(object sender, EventArgs e)
        {
            if (rbStdId.Checked)
            {
                dtViewStudentAttendance.DataSource = StudentRegNoFilter(); ;
            }
        }

        private void rbDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDate.Checked)
            {
                dtViewStudentAttendance.DataSource = DateFilter();
            }
        }
        private DataTable Conversion(DataTable dt)
        {
           
            if(dt.Columns[0].ColumnName == "StudentId")
            {
                foreach(DataRow row in dt.Rows)
                {
                    row["StudentId"] = ucStudent.Instence.getStudentRegNoValue(row.ItemArray[0].ToString());
                }
            }
            if(dt.Columns[0].ColumnName == "AttendanceStatus")
            {
                foreach (DataRow row in dt.Rows)
                {
                    row["AttendanceStatus"] = ucStudentAttendance.Instance.getStatusValue(row.ItemArray[0].ToString());
                }
            }
            if(dt.Columns[0].ColumnName == "AttendanceId")
            {
                foreach (DataRow row in dt.Rows)
                {
                    row["AttendanceId"] = ucClassAttendance.Instance.getClassAttendanceDate(row.ItemArray[0].ToString());
                }
            }
            return dt;
        }

        private void rbMonthWise_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMonthWise.Checked)
            {
                dtViewStudentAttendance.DataSource = MonthWiseFilter(); ;
            }
        }

        private void rbMontWiseEachStudent_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMontWiseEachStudent.Checked)
            {
                dtViewStudentAttendance.DataSource = MonthWiseEachStudentFilter(); ;
            }
        }

        private void rbPercentage_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPercentage.Checked)
            {
                dtViewStudentAttendance.DataSource = MonthWisePercentage(); ;
            }
        }

        private void rbDailyAttendancePers_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDailyAttendancePers.Checked)
            {
                dtViewStudentAttendance.DataSource = DailyAttendancePercentage(); ;
            }
        }
    }



}

﻿
namespace DBProject2
{
    partial class ucAssessmentComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucAssessmentComponent));
            this.panel1 = new System.Windows.Forms.Panel();
            this.view = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.numTotalMarks = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddAssessmentComponent = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewAssessmentComponent = new System.Windows.Forms.DataGridView();
            this.cmbAssessmentId = new System.Windows.Forms.ComboBox();
            this.cmbRubricId = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addAssessmentComponentRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshAddAssessmentComp = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAssessmentComponent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.searchAssessmentComp = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalMarks)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewAssessmentComponent)).BeginInit();
            this.addAssessmentComponentRefresh.SuspendLayout();
            this.searchAssessmentComponent.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.view);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(981, 351);
            this.panel1.TabIndex = 1;
            // 
            // view
            // 
            this.view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.view.AutoSize = true;
            this.view.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Location = new System.Drawing.Point(887, 315);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(66, 21);
            this.view.TabIndex = 10;
            this.view.Text = "View All";
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSearch.Location = new System.Drawing.Point(31, 307);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(359, 29);
            this.txtSearch.TabIndex = 9;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(31, 29);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(922, 257);
            this.splitContainer1.SplitterDistance = 502;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ContextMenuStrip = this.addAssessmentComponentRefresh;
            this.panel3.Controls.Add(this.cmbRubricId);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cmbAssessmentId);
            this.panel3.Controls.Add(this.numTotalMarks);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnAddAssessmentComponent);
            this.panel3.Controls.Add(this.txtName);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(502, 257);
            this.panel3.TabIndex = 3;
            // 
            // numTotalMarks
            // 
            this.numTotalMarks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numTotalMarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numTotalMarks.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.numTotalMarks.Location = new System.Drawing.Point(146, 105);
            this.numTotalMarks.Name = "numTotalMarks";
            this.numTotalMarks.Size = new System.Drawing.Size(323, 27);
            this.numTotalMarks.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Assessment ID:";
            // 
            // btnAddAssessmentComponent
            // 
            this.btnAddAssessmentComponent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAssessmentComponent.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAssessmentComponent.Location = new System.Drawing.Point(23, 199);
            this.btnAddAssessmentComponent.Name = "btnAddAssessmentComponent";
            this.btnAddAssessmentComponent.Size = new System.Drawing.Size(445, 38);
            this.btnAddAssessmentComponent.TabIndex = 4;
            this.btnAddAssessmentComponent.Text = "Add";
            this.btnAddAssessmentComponent.UseVisualStyleBackColor = true;
            this.btnAddAssessmentComponent.Click += new System.EventHandler(this.btnAddAssessmentComponent_Click);
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(146, 21);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(322, 27);
            this.txtName.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Total Marks:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Name:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.searchAssessmentComponent;
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(416, 257);
            this.panel4.TabIndex = 4;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(24, 176);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(369, 39);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtSearchValue.Location = new System.Drawing.Point(146, 119);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(244, 27);
            this.txtSearchValue.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "Name",
            "RubricId",
            "TotalMarks",
            "DateCreated",
            "DateUpdated",
            "AssessmentId"});
            this.cmbColumn.Location = new System.Drawing.Point(146, 37);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(244, 28);
            this.cmbColumn.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtViewAssessmentComponent);
            this.panel2.Location = new System.Drawing.Point(2, 352);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(981, 276);
            this.panel2.TabIndex = 2;
            // 
            // dtViewAssessmentComponent
            // 
            this.dtViewAssessmentComponent.AllowUserToAddRows = false;
            this.dtViewAssessmentComponent.AllowUserToResizeColumns = false;
            this.dtViewAssessmentComponent.AllowUserToResizeRows = false;
            this.dtViewAssessmentComponent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewAssessmentComponent.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewAssessmentComponent.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewAssessmentComponent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewAssessmentComponent.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewAssessmentComponent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewAssessmentComponent.ColumnHeadersHeight = 35;
            this.dtViewAssessmentComponent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewAssessmentComponent.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewAssessmentComponent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewAssessmentComponent.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewAssessmentComponent.Location = new System.Drawing.Point(0, 0);
            this.dtViewAssessmentComponent.Name = "dtViewAssessmentComponent";
            this.dtViewAssessmentComponent.ReadOnly = true;
            this.dtViewAssessmentComponent.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewAssessmentComponent.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewAssessmentComponent.RowHeadersVisible = false;
            this.dtViewAssessmentComponent.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewAssessmentComponent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtViewAssessmentComponent.Size = new System.Drawing.Size(981, 276);
            this.dtViewAssessmentComponent.TabIndex = 12;
            this.dtViewAssessmentComponent.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtViewAssessmentComponent_CellClick);
            // 
            // cmbAssessmentId
            // 
            this.cmbAssessmentId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAssessmentId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbAssessmentId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAssessmentId.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbAssessmentId.FormattingEnabled = true;
            this.cmbAssessmentId.Location = new System.Drawing.Point(147, 147);
            this.cmbAssessmentId.Name = "cmbAssessmentId";
            this.cmbAssessmentId.Size = new System.Drawing.Size(322, 28);
            this.cmbAssessmentId.TabIndex = 11;
            // 
            // cmbRubricId
            // 
            this.cmbRubricId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbRubricId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbRubricId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbRubricId.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbRubricId.FormattingEnabled = true;
            this.cmbRubricId.Location = new System.Drawing.Point(146, 61);
            this.cmbRubricId.Name = "cmbRubricId";
            this.cmbRubricId.Size = new System.Drawing.Size(322, 28);
            this.cmbRubricId.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "Rubric ID:";
            // 
            // addAssessmentComponentRefresh
            // 
            this.addAssessmentComponentRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshAddAssessmentComp});
            this.addAssessmentComponentRefresh.Name = "addRubricLevelRefresh";
            this.addAssessmentComponentRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshAddAssessmentComp
            // 
            this.refreshAddAssessmentComp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshAddAssessmentComp.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshAddAssessmentComp.Image = ((System.Drawing.Image)(resources.GetObject("refreshAddAssessmentComp.Image")));
            this.refreshAddAssessmentComp.Name = "refreshAddAssessmentComp";
            this.refreshAddAssessmentComp.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshAddAssessmentComp.Size = new System.Drawing.Size(157, 26);
            this.refreshAddAssessmentComp.Text = "Refresh";
            this.refreshAddAssessmentComp.Click += new System.EventHandler(this.refreshAddAssessmentComp_Click);
            // 
            // searchAssessmentComponent
            // 
            this.searchAssessmentComponent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchAssessmentComp});
            this.searchAssessmentComponent.Name = "addRubricLevelRefresh";
            this.searchAssessmentComponent.Size = new System.Drawing.Size(158, 30);
            // 
            // searchAssessmentComp
            // 
            this.searchAssessmentComp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.searchAssessmentComp.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.searchAssessmentComp.Image = ((System.Drawing.Image)(resources.GetObject("searchAssessmentComp.Image")));
            this.searchAssessmentComp.Name = "searchAssessmentComp";
            this.searchAssessmentComp.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.searchAssessmentComp.Size = new System.Drawing.Size(157, 26);
            this.searchAssessmentComp.Text = "Refresh";
            this.searchAssessmentComp.Click += new System.EventHandler(this.searchAssessmentComp_Click);
            // 
            // ucAssessmentComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucAssessmentComponent";
            this.Size = new System.Drawing.Size(984, 632);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalMarks)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewAssessmentComponent)).EndInit();
            this.addAssessmentComponentRefresh.ResumeLayout(false);
            this.searchAssessmentComponent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label view;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.NumericUpDown numTotalMarks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddAssessmentComponent;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbRubricId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbAssessmentId;
        private System.Windows.Forms.DataGridView dtViewAssessmentComponent;
        private System.Windows.Forms.ContextMenuStrip addAssessmentComponentRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshAddAssessmentComp;
        private System.Windows.Forms.ContextMenuStrip searchAssessmentComponent;
        private System.Windows.Forms.ToolStripMenuItem searchAssessmentComp;
    }
}

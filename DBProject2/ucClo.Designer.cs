﻿
namespace DBProject2
{
    partial class ucClo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucClo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SearchRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshSearchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.viewAllClos = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CloRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshCloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddClo = new System.Windows.Forms.Button();
            this.txtCloName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewClos = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SearchRefresh.SuspendLayout();
            this.panel3.SuspendLayout();
            this.CloRefresh.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewClos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.viewAllClos);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 355);
            this.panel1.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSearch.Location = new System.Drawing.Point(30, 306);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(359, 29);
            this.txtSearch.TabIndex = 14;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.SearchRefresh;
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(431, 256);
            this.panel4.TabIndex = 4;
            // 
            // SearchRefresh
            // 
            this.SearchRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshSearchToolStripMenuItem1});
            this.SearchRefresh.Name = "SearchRefresh";
            this.SearchRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshSearchToolStripMenuItem1
            // 
            this.refreshSearchToolStripMenuItem1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshSearchToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshSearchToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("refreshSearchToolStripMenuItem1.Image")));
            this.refreshSearchToolStripMenuItem1.Name = "refreshSearchToolStripMenuItem1";
            this.refreshSearchToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshSearchToolStripMenuItem1.Size = new System.Drawing.Size(157, 26);
            this.refreshSearchToolStripMenuItem1.Text = "Refresh";
            this.refreshSearchToolStripMenuItem1.Click += new System.EventHandler(this.refreshSearchToolStripMenuItem1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(23, 176);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(377, 37);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtSearchValue.Location = new System.Drawing.Point(138, 98);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(262, 27);
            this.txtSearchValue.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "Name",
            "DateCreated",
            "DateUpdated"});
            this.cmbColumn.Location = new System.Drawing.Point(138, 45);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(262, 28);
            this.cmbColumn.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // viewAllClos
            // 
            this.viewAllClos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.viewAllClos.AutoSize = true;
            this.viewAllClos.Font = new System.Drawing.Font("Segoe UI", 13F);
            this.viewAllClos.Location = new System.Drawing.Point(879, 310);
            this.viewAllClos.Name = "viewAllClos";
            this.viewAllClos.Size = new System.Drawing.Size(74, 25);
            this.viewAllClos.TabIndex = 3;
            this.viewAllClos.Text = "View All";
            this.viewAllClos.Click += new System.EventHandler(this.viewAllClos_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ContextMenuStrip = this.CloRefresh;
            this.panel3.Controls.Add(this.btnAddClo);
            this.panel3.Controls.Add(this.txtCloName);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(494, 256);
            this.panel3.TabIndex = 1;
            // 
            // CloRefresh
            // 
            this.CloRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshCloToolStripMenuItem});
            this.CloRefresh.Name = "CloRefresh";
            this.CloRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshCloToolStripMenuItem
            // 
            this.refreshCloToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshCloToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshCloToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("refreshCloToolStripMenuItem.Image")));
            this.refreshCloToolStripMenuItem.Name = "refreshCloToolStripMenuItem";
            this.refreshCloToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshCloToolStripMenuItem.Size = new System.Drawing.Size(157, 26);
            this.refreshCloToolStripMenuItem.Text = "Refresh";
            this.refreshCloToolStripMenuItem.Click += new System.EventHandler(this.refreshCloToolStripMenuItem_Click);
            // 
            // btnAddClo
            // 
            this.btnAddClo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddClo.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddClo.Location = new System.Drawing.Point(20, 167);
            this.btnAddClo.Name = "btnAddClo";
            this.btnAddClo.Size = new System.Drawing.Size(439, 39);
            this.btnAddClo.TabIndex = 7;
            this.btnAddClo.Text = "Add";
            this.btnAddClo.UseVisualStyleBackColor = true;
            this.btnAddClo.Click += new System.EventHandler(this.btnAddClo_Click);
            // 
            // txtCloName
            // 
            this.txtCloName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCloName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCloName.Location = new System.Drawing.Point(20, 97);
            this.txtCloName.Name = "txtCloName";
            this.txtCloName.Size = new System.Drawing.Size(439, 27);
            this.txtCloName.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label4.Location = new System.Drawing.Point(19, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Name:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.dtViewClos);
            this.panel2.Location = new System.Drawing.Point(2, 361);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(981, 268);
            this.panel2.TabIndex = 1;
            // 
            // dtViewClos
            // 
            this.dtViewClos.AllowUserToAddRows = false;
            this.dtViewClos.AllowUserToResizeColumns = false;
            this.dtViewClos.AllowUserToResizeRows = false;
            this.dtViewClos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewClos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewClos.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewClos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewClos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewClos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewClos.ColumnHeadersHeight = 35;
            this.dtViewClos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewClos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewClos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewClos.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewClos.Location = new System.Drawing.Point(0, 0);
            this.dtViewClos.Name = "dtViewClos";
            this.dtViewClos.ReadOnly = true;
            this.dtViewClos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewClos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewClos.RowHeadersVisible = false;
            this.dtViewClos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewClos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtViewClos.Size = new System.Drawing.Size(981, 268);
            this.dtViewClos.TabIndex = 16;
            this.dtViewClos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtViewClos_CellClick);
            this.dtViewClos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtViewClos_CellDoubleClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(31, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(929, 256);
            this.splitContainer1.SplitterDistance = 494;
            this.splitContainer1.TabIndex = 15;
            // 
            // ucClo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucClo";
            this.Size = new System.Drawing.Size(984, 632);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.SearchRefresh.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.CloRefresh.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewClos)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dtViewClos;
        private System.Windows.Forms.Label viewAllClos;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnAddClo;
        private System.Windows.Forms.TextBox txtCloName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ContextMenuStrip CloRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshCloToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip SearchRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshSearchToolStripMenuItem1;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

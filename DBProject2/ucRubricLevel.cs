﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucRubricLevel : UserControl
    {
        private static ucRubricLevel _instance;
        private static String rubricLevelIndex = "";
        public static ucRubricLevel Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucRubricLevel();
                }
                return _instance;
            }
        }
        private ucRubricLevel()
        {
            InitializeComponent();
        }

        private void btnAddRubricLevel_Click(object sender, EventArgs e)
        {
            string detail = "";
            if (txtDetail.Text != "")
            {
                detail = txtDetail.Text;
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String rubricId = "";
            if (cmbRubric.SelectedIndex != -1)
            {
                String rubricName = cmbRubric.SelectedItem.ToString();
                rubricId = ucRubric.Instance.getRubricIndex(rubricName);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            String measurementlevel = "";
            if(numMeasurementLevel.Value != 0)
            {
                measurementlevel = numMeasurementLevel.Value.ToString();
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }

            if (detail != "" && rubricId != "" && measurementlevel != "")
            {
                if (btnAddRubricLevel.Text == "Add")
                {
                    
                    Insert(detail, rubricId,measurementlevel);
                }
                if (btnAddRubricLevel.Text == "Update")
                {
                    Update(detail, rubricId,measurementlevel,rubricLevelIndex);
                }
                dtViewRubricLevel.DataSource = Search("", "");

            }
        }
        private void Insert(String detail, String rubricId, String measureLevel)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO RubricLevel VALUES("+rubricId+","+"'"+detail+"', "+measureLevel+")",con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        private void Update(String detail, String rubricId, String measureLevel, String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE RubricLevel SET Details= '"+detail+"', RubricId = "+rubricId+", MeasurementLevel =  "+measureLevel+" WHERE id = "+index,con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Updated");
        }
        public DataTable Search(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if(column != "" && val != "")
            {
                cmd = new SqlCommand("SELECT id,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id), Details,MeasurementLevel FROM RubricLevel WHERE (SELECT Details FROM Rubric WHERE RubricId = id) = '"+val+"' OR " + column+" = '"+val+"'", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT id,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id), Details,MeasurementLevel FROM RubricLevel", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        public void getRubrics()
        {
            cmbRubric.Items.Clear();
            ucRubric.Instance.getRubrics(cmbRubric);
        }
        public void getRubricLevels(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Details FROM RubricLevel", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getRubricLevelValue(String index)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Details FROM RubricLevel WHERE id = " + index, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
        public String getRubricLevelIndex(String val)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM RubricLevel WHERE Details = '" + val + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
        private void view_Click(object sender, EventArgs e)
        {
            dtViewRubricLevel.DataSource = Search("","");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewRubricLevel.DataSource = Search(column, value);
                
            }

        }
        private void Refresh(String option)
        {
            if (option == "Add")
            {
                txtDetail.Text = "";
                cmbRubric.Text = "";
                cmbRubric.SelectedIndex = -1;
                numMeasurementLevel.Value = 0;
                btnAddRubricLevel.Text = "Add";
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id), Details,MeasurementLevel FROM RubricLevel WHERE id LIKE '%" + str + "%' OR Details LIKE '%" + str + "%' OR (SELECT Details FROM Rubric WHERE RubricId = id) LIKE '%" + str + "%' OR MeasurementLevel LIKE '%"+str+"'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String str = txtSearch.Text;
            dtViewRubricLevel.DataSource = IsContains(str);
        }

        private void dtViewRubricLevel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dt = dtViewRubricLevel.Rows[0];
            String index = "";
            try
            {
                dt = dtViewRubricLevel.Rows[e.RowIndex];
                index = dt.Cells[0].Value.ToString();
            }
            catch (Exception ex) { }
            if (index != "")
            {
               
                // Get Rubric Name of relate index From Rubric table
               // DataTable d = ucRubric.Instance.Search("id", dt.Cells[0].Value.ToString());
                txtDetail.Text = dt.Cells[2].Value.ToString();
                cmbRubric.SelectedItem = dt.Cells[1].Value.ToString();
                numMeasurementLevel.Value = Decimal.Parse(dt.Cells[3].Value.ToString());
                rubricLevelIndex = index;
                btnAddRubricLevel.Text = "Update";
            }
        }

        private void refreshAddRubricLevel_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void refreshSearchRubricLevel_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }
    }
}

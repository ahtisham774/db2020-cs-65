﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void manageStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucStudent.Instence);
            ucStudent.Instence.Dock = DockStyle.Fill;
            ucStudent.Instence.BringToFront();

        }

        private void manageCLOsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucClo.Instance);
            ucClo.Instance.Dock = DockStyle.Fill;
            ucClo.Instance.BringToFront();
        }

        private void manageRubricToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void manageAssessmentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void rubricLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucRubricLevel.Instance);
            ucRubricLevel.Instance.Dock = DockStyle.Fill;
            ucRubricLevel.Instance.BringToFront();
            ucRubricLevel.Instance.getRubrics();
        }

        private void rubricToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucRubric.Instance);
            ucRubric.Instance.Dock = DockStyle.Fill;
            ucRubric.Instance.BringToFront();
            ucRubric.Instance.getClos();
        }

        private void assessmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucAssessment.Instance);
            ucAssessment.Instance.Dock = DockStyle.Fill;
            ucAssessment.Instance.BringToFront();
            
        }

        private void assessmentLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucAssessmentComponent.Instance);
            ucAssessmentComponent.Instance.Dock = DockStyle.Fill;
            ucAssessmentComponent.Instance.BringToFront();
            ucAssessmentComponent.Instance.getAssessmentIds();
            ucAssessmentComponent.Instance.getRubricIds();
        }

        private void classAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucClassAttendance.Instance);
            ucClassAttendance.Instance.Dock = DockStyle.Fill;
            ucClassAttendance.Instance.BringToFront();
        }

        private void studentAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucStudentAttendance.Instance);
            ucStudentAttendance.Instance.Dock = DockStyle.Fill;
            ucStudentAttendance.Instance.BringToFront();
            ucStudentAttendance.Instance.getAttendaces();
           
        }

        private void viewAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucViewAttendance.Instance);
            ucViewAttendance.Instance.Dock = DockStyle.Fill;
            ucViewAttendance.Instance.BringToFront();
            
            
        }

        private void resultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucStudentResult.Instance);
            ucStudentResult.Instance.Dock = DockStyle.Fill;
            ucStudentResult.Instance.BringToFront();
            ucStudentResult.Instance.getAssessmentComponents();
            ucStudentResult.Instance.getRubricMeasurements();
            ucStudentResult.Instance.getStudents();

        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(ucReport.Instance);
            ucReport.Instance.Dock = DockStyle.Fill;
            ucReport.Instance.BringToFront();
            ucReport.Instance.getAssessments();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }



        //private void button2_Click(object sender, EventArgs e)
        //{

        //}
    }
}

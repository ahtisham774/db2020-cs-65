﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucAssessment : UserControl
    {
        private static ucAssessment _instance;
        private static String assessmentIndex = "";
        public static ucAssessment Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucAssessment();
                }
                return _instance;
            }
        }
        private ucAssessment()
        {
            InitializeComponent();
        }

        private void btnAddAssessment_Click(object sender, EventArgs e)
        {
            string title = "";
            if (txtTitle.Text != "")
            {
                title = txtTitle.Text;
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String totalMarks = "";
            if (numTotalMarks.Value != 0)
            {
                totalMarks = numTotalMarks.Value.ToString();
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String totalWeightage = "";
            if (numTotalWeightage.Value != 0)
            {
                totalWeightage = numTotalWeightage.Value.ToString();
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }

            if (title != "" && totalMarks != "" && totalWeightage != "")
            {
                if (btnAddAssessment.Text == "Add")
                {
                    var date = DateTime.Now.ToShortDateString();
                   
                   Insert(title, totalMarks, totalWeightage,date);
                }
                if (btnAddAssessment.Text == "Update")
                {
                   Update(title, totalMarks, totalWeightage,assessmentIndex);
                }
                dtViewAssessment.DataSource = Search("", "");

            }
        }
        private void Insert(String title, String totalMarks, String totalWeightage, String date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Assessment VALUES('"+title+"','"+date+"',"+totalMarks+","+totalWeightage+")",con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        private void Update(String title, String totalMarks, String totalWeightage,String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title = '" + title + "', TotalMarks = " + totalMarks + ", TotalWeightage = " + totalWeightage +" WHERE id = "+index, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Updated");
        }
        private void refreshAddAssessment_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void refreshSearchAssessmenr_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }
        private void Refresh(String option)
        {
            if (option == "Add")
            {
                numTotalWeightage.Value = 0;
                txtTitle.Text = "";              
                numTotalMarks.Value = 0;                
                btnAddAssessment.Text = "Add";
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }
        public DataTable Search(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if(column != "" && val != "")
            {
                cmd = new SqlCommand("SELECT * FROM Assessment WHERE "+column+"= '"+val+"'", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT * FROM Assessment", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }

        private void view_Click(object sender, EventArgs e)
        {
            dtViewAssessment.DataSource = Search("","");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewAssessment.DataSource = Search(column, value);
                
            }
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE id LIKE '%" + str + "%' OR Title LIKE '%" + str + "%' OR TotalMarks LIKE '%" + str + "%' OR TotalWeightage LIKE '%" + str + "%' OR DateCreated LIKE '%"+str+"%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String str = txtSearch.Text;
            dtViewAssessment.DataSource = IsContains(str);
        }

        private void dtViewAssessment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dt =  dtViewAssessment.Rows[0]; 
            String index = "";
            try
            {
                dt = dtViewAssessment.Rows[e.RowIndex];
                index = dt.Cells[0].Value.ToString();
            }
            catch (Exception ex) { }
            if (index != "")
            {
                txtTitle.Text = dt.Cells[1].Value.ToString();
                numTotalMarks.Value = Decimal.Parse(dt.Cells[3].Value.ToString());                
                numTotalWeightage.Value = Decimal.Parse(dt.Cells[4].Value.ToString());
                assessmentIndex = index;
                btnAddAssessment.Text = "Update";
            }
        }
        public String getAssessmentIndex(String name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM Assessment WHERE Title = '"+name+"'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
        public String getAssessmentValue(String id)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Title FROM Assessment WHERE id = " + id , con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
        public void getAssessements(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Title FROM Assessment", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());
                
            }

        }
        public DataTable AssessmentWiseResult()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT A.Title, COUNT(SR.StudentId) AS NumberOfStudents,(SELECT COUNT(*) FROM (SELECT Id FROM Student) AS C) TotalStudents,FLOOR((COUNT( SR.StudentId)*1.0/(SELECT COUNT(*) FROM (SELECT  id FROM Student) AS C))*100) AS ClassResult FROM Student S INNER JOIN  StudentResult SR ON S.Id = SR.StudentId INNER JOIN AssessmentComponent AC ON SR.AssessmentComponentId = AC.Id RIGHT OUTER JOIN Assessment A ON AC.AssessmentId = A.Id GROUP BY A.Title", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
    }
}

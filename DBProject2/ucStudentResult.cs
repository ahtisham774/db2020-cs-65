﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucStudentResult : UserControl
    {
        private static ucStudentResult _instance;
        public static ucStudentResult Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucStudentResult();
                }
                return _instance;
            }
        }
        private  ucStudentResult()
        {
            InitializeComponent();
        }

        private void btnAddStudentResult_Click(object sender, EventArgs e)
        {
            String studentId = "";
            if (cmbStudent.SelectedIndex != -1)
            {
                String studentRegNo = cmbStudent.SelectedItem.ToString();
                studentId = ucStudent.Instence.getStudentIndex(studentRegNo);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            String rmeasurementLevel = "";
            if (cmbRubricMeasurement.SelectedIndex != -1)
            {
                String rmeasurement = cmbRubricMeasurement.SelectedItem.ToString();
                rmeasurementLevel = ucRubricLevel.Instance.getRubricLevelIndex(rmeasurement);
               

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            String assessmentComponent = "";
            if (cmbAssessmentComponent.SelectedIndex != -1)
            {
                String assessment = cmbAssessmentComponent.SelectedItem.ToString();
                
                assessmentComponent = ucAssessmentComponent.Instance.getAssessmentComponentIndex(assessment);
               
            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            if (studentId != "" && assessmentComponent != "" && rmeasurementLevel != "")
            {
                if (btnAddStudentResult.Text == "Add")
                {
                    String date = DateTime.Now.ToShortDateString();
                    //MessageBox.Show(studentId + "\n" + rmeasurementLevel + "\n" + assessmentComponent);
                    Insert(studentId,rmeasurementLevel,assessmentComponent,date);
                    Refresh("Add");
                }
                if (btnAddStudentResult.Text == "Update")
                {
                   // Update(detail, rubricId, measurementlevel, rubricLevelIndex);
                }
                dtViewStudentResult.DataSource = Search("", "");

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewStudentResult.DataSource = Search(column, value);

            }
        }

        public void getStudents()
        {
            cmbStudent.Items.Clear();
            ucStudent.Instence.getStudentRego(cmbStudent);
        }
        public void getRubricMeasurements()
        {
            cmbRubricMeasurement.Items.Clear();
            ucRubricLevel.Instance.getRubricLevels(cmbRubricMeasurement);
        }
        public void getAssessmentComponents()
        {
            cmbAssessmentComponent.Items.Clear();
            ucAssessmentComponent.Instance.getAssessmentComponents(cmbAssessmentComponent);
        }

        private void view_Click(object sender, EventArgs e)
        {
            dtViewStudentResult.DataSource = Search("", "");
        }
        private void Insert( String stdId, String measureLevel,String assessmentComponent,String date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentResult VALUES(" + stdId + ","  + assessmentComponent + ", " + measureLevel +",'"+ date+"')", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        public DataTable Search(String column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if (column != "" && val != "")
            {
                cmd = new SqlCommand("SELECT StudentId = (SELECT RegistrationNumber FROM Student WHERE StudentId = id),AssessmentComponentId = (SELECT Name FROM AssessmentComponent WHERE AssessmentComponentId = id), RubricMeasurementId = (SELECT Details FROM RubricLevel WHERE RubricMeasurementId = id) FROM StudentResult WHERE (SELECT RegistrationNumber FROM Student WHERE StudentId = id) = '"+val+"' OR (SELECT Name FROM AssessmentComponent WHERE AssessmentComponentId = id) = '"+val+"' OR (SELECT Details FROM RubricLevel WHERE RubricMeasurementId = id) = '"+val+"' OR "+column+"= '"+val+"'", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT StudentId = (SELECT RegistrationNumber FROM Student WHERE StudentId = id),AssessmentComponentId = (SELECT Name FROM AssessmentComponent WHERE AssessmentComponentId = id), RubricMeasurementId = (SELECT Details FROM RubricLevel WHERE RubricMeasurementId = id) FROM StudentResult", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private void Refresh(String option)
        {
            if (option == "Add")
            {
                cmbAssessmentComponent.Text = "";
                cmbRubricMeasurement.Text = "";
                cmbStudent.Text = "";
                cmbRubricMeasurement.SelectedIndex = -1;
                cmbAssessmentComponent.SelectedIndex = -1;
                cmbStudent.SelectedIndex = -1;
                btnAddStudentResult.Text = "Add";
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }

        private void refreshAddRubricLevel_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void refreshSearchRubricLevel_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }
        public DataTable StudentsWhoAttends(String assmentType)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT S.RegistrationNumber,result.Title FROM Student S LEFT OUTER JOIN  (SELECT SR.StudentId,A.Title FROM  StudentResult SR INNER JOIN AssessmentComponent AC ON SR.AssessmentComponentId = AC.Id INNER JOIN Assessment A ON AC.AssessmentId = A.Id  WHERE A.Title = '"+assmentType+"') result ON S.Id = result.StudentId WHERE result.StudentId IS NOT NULL", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        public DataTable StudentsWhoNotAttends(String assmentType)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT S.RegistrationNumber,result.Title FROM Student S LEFT OUTER JOIN  (SELECT SR.StudentId,A.Title FROM  StudentResult SR INNER JOIN AssessmentComponent AC ON SR.AssessmentComponentId = AC.Id INNER JOIN Assessment A ON AC.AssessmentId = A.Id  WHERE A.Title = '"+assmentType+"') result ON S.Id = result.StudentId WHERE result.StudentId IS NULL", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        public DataTable ClassResult()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" SELECT S.RegistrationNumber,result.Title AS AssessmentTitle,result.TotalMarks,result.TotalWeightage,result.Name AS CLOS,FORMAT(result.EvaluationDate,'dd-MMMM-yyy')AS EvaluationDate FROM Student S LEFT OUTER JOIN (SELECT SR.StudentId,A.Title,A.TotalMarks,A.TotalWeightage,C.Name,SR.EvaluationDate FROM StudentResult SR INNER JOIN AssessmentComponent AC ON SR.AssessmentComponentId = AC.Id INNER JOIN Assessment A ON A.Id = AC.AssessmentId INNER JOIN RubricLevel RL ON SR.RubricMeasurementId = RL.Id INNER JOIN Rubric R ON RL.RubricId = R.Id INNER JOIN Clo C ON R.CloId = C.Id) AS result ON S.Id = result.StudentId", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
    }
}

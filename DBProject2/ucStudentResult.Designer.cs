﻿
namespace DBProject2
{
    partial class ucStudentResult
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStudentResult));
            this.view = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbStudent = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbAssessmentComponent = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddStudentResult = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSearchValue = new System.Windows.Forms.TextBox();
            this.cmbRubricMeasurement = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewStudentResult = new System.Windows.Forms.DataGridView();
            this.addRubricLevelRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshAddRubricLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchRubricLevelRefresh = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshSearchRubricLevel = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewStudentResult)).BeginInit();
            this.addRubricLevelRefresh.SuspendLayout();
            this.SearchRubricLevelRefresh.SuspendLayout();
            this.SuspendLayout();
            // 
            // view
            // 
            this.view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.view.AutoSize = true;
            this.view.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Location = new System.Drawing.Point(885, 314);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(66, 21);
            this.view.TabIndex = 15;
            this.view.Text = "View All";
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(25, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(935, 256);
            this.splitContainer1.SplitterDistance = 524;
            this.splitContainer1.TabIndex = 16;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.SearchRubricLevelRefresh;
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(407, 256);
            this.panel4.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(28, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "StudentId",
            "AssessmentComponentId",
            "RubricMeasurementId",
            "EvaluationDate"});
            this.cmbColumn.Location = new System.Drawing.Point(175, 49);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(191, 28);
            this.cmbColumn.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(33, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(37, 190);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(329, 37);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.view);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 355);
            this.panel1.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.ContextMenuStrip = this.addRubricLevelRefresh;
            this.panel3.Controls.Add(this.cmbRubricMeasurement);
            this.panel3.Controls.Add(this.cmbStudent);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cmbAssessmentComponent);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btnAddStudentResult);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(524, 256);
            this.panel3.TabIndex = 4;
            // 
            // cmbStudent
            // 
            this.cmbStudent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbStudent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbStudent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbStudent.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbStudent.FormattingEnabled = true;
            this.cmbStudent.Location = new System.Drawing.Point(146, 29);
            this.cmbStudent.Name = "cmbStudent";
            this.cmbStudent.Size = new System.Drawing.Size(344, 28);
            this.cmbStudent.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "Student:";
            // 
            // cmbAssessmentComponent
            // 
            this.cmbAssessmentComponent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAssessmentComponent.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbAssessmentComponent.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAssessmentComponent.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbAssessmentComponent.FormattingEnabled = true;
            this.cmbAssessmentComponent.Location = new System.Drawing.Point(209, 135);
            this.cmbAssessmentComponent.Name = "cmbAssessmentComponent";
            this.cmbAssessmentComponent.Size = new System.Drawing.Size(282, 28);
            this.cmbAssessmentComponent.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Assessment Component:";
            // 
            // btnAddStudentResult
            // 
            this.btnAddStudentResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddStudentResult.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStudentResult.Location = new System.Drawing.Point(23, 193);
            this.btnAddStudentResult.Name = "btnAddStudentResult";
            this.btnAddStudentResult.Size = new System.Drawing.Size(467, 38);
            this.btnAddStudentResult.TabIndex = 4;
            this.btnAddStudentResult.Text = "Add";
            this.btnAddStudentResult.UseVisualStyleBackColor = true;
            this.btnAddStudentResult.Click += new System.EventHandler(this.btnAddStudentResult_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Rubric Measurement:";
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtSearchValue.Location = new System.Drawing.Point(90, 109);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(279, 27);
            this.txtSearchValue.TabIndex = 13;
            // 
            // cmbRubricMeasurement
            // 
            this.cmbRubricMeasurement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbRubricMeasurement.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbRubricMeasurement.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbRubricMeasurement.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbRubricMeasurement.FormattingEnabled = true;
            this.cmbRubricMeasurement.Location = new System.Drawing.Point(208, 83);
            this.cmbRubricMeasurement.Name = "cmbRubricMeasurement";
            this.cmbRubricMeasurement.Size = new System.Drawing.Size(282, 28);
            this.cmbRubricMeasurement.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtViewStudentResult);
            this.panel2.Location = new System.Drawing.Point(2, 355);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(982, 277);
            this.panel2.TabIndex = 3;
            // 
            // dtViewStudentResult
            // 
            this.dtViewStudentResult.AllowUserToAddRows = false;
            this.dtViewStudentResult.AllowUserToResizeColumns = false;
            this.dtViewStudentResult.AllowUserToResizeRows = false;
            this.dtViewStudentResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewStudentResult.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewStudentResult.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewStudentResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewStudentResult.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewStudentResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewStudentResult.ColumnHeadersHeight = 35;
            this.dtViewStudentResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewStudentResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewStudentResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewStudentResult.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewStudentResult.Location = new System.Drawing.Point(0, 0);
            this.dtViewStudentResult.Name = "dtViewStudentResult";
            this.dtViewStudentResult.ReadOnly = true;
            this.dtViewStudentResult.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewStudentResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewStudentResult.RowHeadersVisible = false;
            this.dtViewStudentResult.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewStudentResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtViewStudentResult.Size = new System.Drawing.Size(982, 277);
            this.dtViewStudentResult.TabIndex = 20;
            // 
            // addRubricLevelRefresh
            // 
            this.addRubricLevelRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshAddRubricLevel});
            this.addRubricLevelRefresh.Name = "addRubricLevelRefresh";
            this.addRubricLevelRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshAddRubricLevel
            // 
            this.refreshAddRubricLevel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshAddRubricLevel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshAddRubricLevel.Image = ((System.Drawing.Image)(resources.GetObject("refreshAddRubricLevel.Image")));
            this.refreshAddRubricLevel.Name = "refreshAddRubricLevel";
            this.refreshAddRubricLevel.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshAddRubricLevel.Size = new System.Drawing.Size(157, 26);
            this.refreshAddRubricLevel.Text = "Refresh";
            this.refreshAddRubricLevel.Click += new System.EventHandler(this.refreshAddRubricLevel_Click);
            // 
            // SearchRubricLevelRefresh
            // 
            this.SearchRubricLevelRefresh.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshSearchRubricLevel});
            this.SearchRubricLevelRefresh.Name = "addRubricLevelRefresh";
            this.SearchRubricLevelRefresh.Size = new System.Drawing.Size(158, 30);
            // 
            // refreshSearchRubricLevel
            // 
            this.refreshSearchRubricLevel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.refreshSearchRubricLevel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.refreshSearchRubricLevel.Image = ((System.Drawing.Image)(resources.GetObject("refreshSearchRubricLevel.Image")));
            this.refreshSearchRubricLevel.Name = "refreshSearchRubricLevel";
            this.refreshSearchRubricLevel.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshSearchRubricLevel.Size = new System.Drawing.Size(157, 26);
            this.refreshSearchRubricLevel.Text = "Refresh";
            this.refreshSearchRubricLevel.Click += new System.EventHandler(this.refreshSearchRubricLevel_Click);
            // 
            // ucStudentResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucStudentResult";
            this.Size = new System.Drawing.Size(984, 632);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewStudentResult)).EndInit();
            this.addRubricLevelRefresh.ResumeLayout(false);
            this.SearchRubricLevelRefresh.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label view;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbStudent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbAssessmentComponent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddStudentResult;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbRubricMeasurement;
        private System.Windows.Forms.TextBox txtSearchValue;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dtViewStudentResult;
        private System.Windows.Forms.ContextMenuStrip addRubricLevelRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshAddRubricLevel;
        private System.Windows.Forms.ContextMenuStrip SearchRubricLevelRefresh;
        private System.Windows.Forms.ToolStripMenuItem refreshSearchRubricLevel;
    }
}

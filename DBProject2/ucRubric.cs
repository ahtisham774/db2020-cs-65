﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucRubric : UserControl
    {
        
        private static ucRubric _instence;
        private static int id = 0;
        private String rubricIndex = "";
        public static ucRubric Instance
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucRubric();
                }
                return _instence;
            }
        }
        private ucRubric()
        {
            InitializeComponent();

        }

        private void Refresh(String option)
        {
            if(option == "Add")
            {
                txtDetail.Text = "";
                cmbClo.Text = "";
                cmbClo.SelectedIndex = -1;
                btnAddRubric.Text = "Add";
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }

        private void refreshAddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void RefreshtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }
        public void getClos()
        {
            cmbClo.Items.Clear();
            ucClo.Instance.getClos(cmbClo);
        }
        private void btnAddRubric_Click(object sender, EventArgs e)
        {
            string detail = "";
            if(txtDetail.Text != "")
            {
                detail = txtDetail.Text;
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String CloId = "";
            if(cmbClo.SelectedIndex != -1)
            {
                String CloName = cmbClo.SelectedItem.ToString();
                CloId = getCloIndex(CloName);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            
            
            
            if (detail != "" && CloId != "")
            {
                if(btnAddRubric.Text == "Add")
                {
                    Insert(detail, CloId);
                }
                if(btnAddRubric.Text == "Update")
                {
                    Update(detail, CloId,rubricIndex);
                }
                dtViewRubric.DataSource = Search("", "");
               
            }
        }
        private String getCloIndex(String clo)
        {
            String index = "";
            DataTable dt = ucClo.Instance.Search("Name", clo);
            index = dt.Rows[0].ItemArray[0].ToString();
            return index;
        }
        private String GenerateIndex()
        {
            String index = id + "";
            return index;
        }
        private void Insert(String detail,String cloid)
        {
            String index = GenerateIndex();
            id++;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Rubric VALUES("+index+",'"+detail+"',"+cloid+")",con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");

        }
        public DataTable Search(String Column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;

            if(Column != "")
            {

              cmd = new SqlCommand("SELECT id,Details,CloId = (SELECT Name FROM Clo WHERE CloId = id) FROM Rubric WHERE (SELECT Name FROM Clo WHERE CloId = id) = '"+val+ "' OR Details = '"+val+"'", con);

            }
            else
            {

              cmd = new SqlCommand("SELECT id,Details,CloId = (SELECT Name FROM Clo WHERE CloId = id) FROM Rubric", con);

                
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private void Update(String detail,String cloid,String id)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Rubric SET Details = '"+detail+"', Cloid = '"+cloid+"' WHERE id = "+id,con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Updated");
        }

        private void view_Click(object sender, EventArgs e)
        {
            dtViewRubric.DataSource =  Search("","");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewRubric.DataSource = Search(column, value);
                Refresh("Search");
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String str = txtSearch.Text;
            dtViewRubric.DataSource = IsContains(str);
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id,Details,CloId = (SELECT Name FROM Clo WHERE CloId = id) FROM Rubric WHERE id LIKE '%" + str + "%' OR Details LIKE '%" + str + "%' OR (SELECT Name FROM Clo WHERE CloId = id) LIKE '%" + str + "%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }

        private void dtViewRubric_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dt = dtViewRubric.Rows[0];
            String index = "";
            try
            {
                dt = dtViewRubric.Rows[e.RowIndex];
                index = dt.Cells[0].Value.ToString();
            }
            catch(Exception ex) { }
            if(index != "")
            {
                txtDetail.Text = dt.Cells[1].Value.ToString();
                // Get Clo Name of relate index From Clo table
              //  DataTable d = ucClo.Instance.Search("id", dt.Cells[2].Value.ToString());
                cmbClo.SelectedItem = dt.Cells[2].Value.ToString();
                rubricIndex = index;
                btnAddRubric.Text = "Update";
            }

        }
        public void getRubrics(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Details FROM Rubric", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getRubricIndex(String rubric)
        {
            String index = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM Rubric WHERE Details = '"+rubric+"'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            index = dt.Rows[0].ItemArray[0].ToString();         
            return index;
        }
        public String getRubricValue(String index)
        {
            
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Details FROM Rubric WHERE id = " + index , con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);             
            return dt.Rows[0].ItemArray[0].ToString();
        }
        public DataTable Rubrics()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" SELECT AC.Name AS Component,R.Details AS Rubric,AC.TotalMarks AS [Component Marks],A.Title AS [Assessment Title],A.TotalMarks AS[Total Marks],A.TotalWeightage AS [Total Weightage],C.Name AS CLO FROM Assessment A INNER JOIN AssessmentComponent AC ON A.Id = AC.AssessmentId INNER JOIN Rubric R ON AC.RubricId = R.Id INNER JOIN Clo C ON R.CloId = C.Id;", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
    }
}

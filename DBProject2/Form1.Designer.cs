﻿
namespace DBProject2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.manageStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageCLOsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageRubricToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rubricToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rubricLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageAssessmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assessmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assessmentLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageStudentsToolStripMenuItem,
            this.manageCLOsToolStripMenuItem,
            this.manageRubricToolStripMenuItem,
            this.manageAssessmentsToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.resultToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(984, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // manageStudentsToolStripMenuItem
            // 
            this.manageStudentsToolStripMenuItem.Name = "manageStudentsToolStripMenuItem";
            this.manageStudentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.manageStudentsToolStripMenuItem.Size = new System.Drawing.Size(142, 25);
            this.manageStudentsToolStripMenuItem.Tag = "";
            this.manageStudentsToolStripMenuItem.Text = "Manage Students";
            this.manageStudentsToolStripMenuItem.Click += new System.EventHandler(this.manageStudentsToolStripMenuItem_Click);
            // 
            // manageCLOsToolStripMenuItem
            // 
            this.manageCLOsToolStripMenuItem.Name = "manageCLOsToolStripMenuItem";
            this.manageCLOsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.manageCLOsToolStripMenuItem.Size = new System.Drawing.Size(118, 25);
            this.manageCLOsToolStripMenuItem.Text = "Manage CLOs";
            this.manageCLOsToolStripMenuItem.Click += new System.EventHandler(this.manageCLOsToolStripMenuItem_Click);
            // 
            // manageRubricToolStripMenuItem
            // 
            this.manageRubricToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rubricToolStripMenuItem,
            this.rubricLevelToolStripMenuItem});
            this.manageRubricToolStripMenuItem.Name = "manageRubricToolStripMenuItem";
            this.manageRubricToolStripMenuItem.Size = new System.Drawing.Size(127, 25);
            this.manageRubricToolStripMenuItem.Text = "Manage Rubric";
            this.manageRubricToolStripMenuItem.Click += new System.EventHandler(this.manageRubricToolStripMenuItem_Click);
            // 
            // rubricToolStripMenuItem
            // 
            this.rubricToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rubricToolStripMenuItem.Name = "rubricToolStripMenuItem";
            this.rubricToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.rubricToolStripMenuItem.Size = new System.Drawing.Size(262, 26);
            this.rubricToolStripMenuItem.Text = "Rubric";
            this.rubricToolStripMenuItem.Click += new System.EventHandler(this.rubricToolStripMenuItem_Click);
            // 
            // rubricLevelToolStripMenuItem
            // 
            this.rubricLevelToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rubricLevelToolStripMenuItem.Name = "rubricLevelToolStripMenuItem";
            this.rubricLevelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.L)));
            this.rubricLevelToolStripMenuItem.Size = new System.Drawing.Size(262, 26);
            this.rubricLevelToolStripMenuItem.Text = "Rubric Level";
            this.rubricLevelToolStripMenuItem.Click += new System.EventHandler(this.rubricLevelToolStripMenuItem_Click);
            // 
            // manageAssessmentsToolStripMenuItem
            // 
            this.manageAssessmentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assessmentToolStripMenuItem,
            this.assessmentLevelToolStripMenuItem});
            this.manageAssessmentsToolStripMenuItem.Name = "manageAssessmentsToolStripMenuItem";
            this.manageAssessmentsToolStripMenuItem.Size = new System.Drawing.Size(171, 25);
            this.manageAssessmentsToolStripMenuItem.Text = "Manage Assessments";
            this.manageAssessmentsToolStripMenuItem.Click += new System.EventHandler(this.manageAssessmentsToolStripMenuItem_Click);
            // 
            // assessmentToolStripMenuItem
            // 
            this.assessmentToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.assessmentToolStripMenuItem.Name = "assessmentToolStripMenuItem";
            this.assessmentToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this.assessmentToolStripMenuItem.Size = new System.Drawing.Size(337, 26);
            this.assessmentToolStripMenuItem.Text = "Assessment";
            this.assessmentToolStripMenuItem.Click += new System.EventHandler(this.assessmentToolStripMenuItem_Click);
            // 
            // assessmentLevelToolStripMenuItem
            // 
            this.assessmentLevelToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.assessmentLevelToolStripMenuItem.Name = "assessmentLevelToolStripMenuItem";
            this.assessmentLevelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this.assessmentLevelToolStripMenuItem.Size = new System.Drawing.Size(337, 26);
            this.assessmentLevelToolStripMenuItem.Text = "AssessmentComponent";
            this.assessmentLevelToolStripMenuItem.Click += new System.EventHandler(this.assessmentLevelToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.classAttendanceToolStripMenuItem,
            this.studentAttendanceToolStripMenuItem,
            this.viewAttendanceToolStripMenuItem});
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(100, 25);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            // 
            // classAttendanceToolStripMenuItem
            // 
            this.classAttendanceToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.classAttendanceToolStripMenuItem.Name = "classAttendanceToolStripMenuItem";
            this.classAttendanceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.classAttendanceToolStripMenuItem.Size = new System.Drawing.Size(352, 26);
            this.classAttendanceToolStripMenuItem.Text = "Class Attendance";
            this.classAttendanceToolStripMenuItem.Click += new System.EventHandler(this.classAttendanceToolStripMenuItem_Click);
            // 
            // studentAttendanceToolStripMenuItem
            // 
            this.studentAttendanceToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.studentAttendanceToolStripMenuItem.Name = "studentAttendanceToolStripMenuItem";
            this.studentAttendanceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.M)));
            this.studentAttendanceToolStripMenuItem.Size = new System.Drawing.Size(352, 26);
            this.studentAttendanceToolStripMenuItem.Text = "Mark Student Attendance";
            this.studentAttendanceToolStripMenuItem.Click += new System.EventHandler(this.studentAttendanceToolStripMenuItem_Click);
            // 
            // viewAttendanceToolStripMenuItem
            // 
            this.viewAttendanceToolStripMenuItem.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.viewAttendanceToolStripMenuItem.Name = "viewAttendanceToolStripMenuItem";
            this.viewAttendanceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.viewAttendanceToolStripMenuItem.Size = new System.Drawing.Size(352, 26);
            this.viewAttendanceToolStripMenuItem.Text = "View Attendance";
            this.viewAttendanceToolStripMenuItem.Click += new System.EventHandler(this.viewAttendanceToolStripMenuItem_Click);
            // 
            // resultToolStripMenuItem
            // 
            this.resultToolStripMenuItem.Name = "resultToolStripMenuItem";
            this.resultToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.resultToolStripMenuItem.Size = new System.Drawing.Size(65, 25);
            this.resultToolStripMenuItem.Text = "Result";
            this.resultToolStripMenuItem.Click += new System.EventHandler(this.resultToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.P)));
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(69, 25);
            this.reportToolStripMenuItem.Text = "Report";
            this.reportToolStripMenuItem.Click += new System.EventHandler(this.reportToolStripMenuItem_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.pictureBox1);
            this.mainPanel.Location = new System.Drawing.Point(0, 29);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(984, 632);
            this.mainPanel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.Font = new System.Drawing.Font("Zilla Slab", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(383, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rubric Base Evaluation";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(984, 632);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1000, 700);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rubric Base Evaluation";
            this.TransparencyKey = System.Drawing.SystemColors.Desktop;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem manageStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageCLOsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageRubricToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageAssessmentsToolStripMenuItem;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem rubricLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rubricToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assessmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assessmentLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
    }
}


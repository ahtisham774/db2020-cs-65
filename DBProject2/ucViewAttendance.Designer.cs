﻿
namespace DBProject2
{
    partial class ucViewAttendance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucViewAttendance));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbStatus = new System.Windows.Forms.RadioButton();
            this.rbStdId = new System.Windows.Forms.RadioButton();
            this.rbDate = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.refreshSearch = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RefreshtoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSearchValue = new System.Windows.Forms.NumericUpDown();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbColumn = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.view = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtViewStudentAttendance = new System.Windows.Forms.DataGridView();
            this.rbMonthWise = new System.Windows.Forms.RadioButton();
            this.rbMontWiseEachStudent = new System.Windows.Forms.RadioButton();
            this.rbPercentage = new System.Windows.Forms.RadioButton();
            this.rbDailyAttendancePers = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.refreshSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchValue)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtViewStudentAttendance)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.view);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 355);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(25, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(935, 256);
            this.splitContainer1.SplitterDistance = 433;
            this.splitContainer1.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.rbDailyAttendancePers);
            this.panel3.Controls.Add(this.rbPercentage);
            this.panel3.Controls.Add(this.rbMontWiseEachStudent);
            this.panel3.Controls.Add(this.rbMonthWise);
            this.panel3.Controls.Add(this.rbStatus);
            this.panel3.Controls.Add(this.rbStdId);
            this.panel3.Controls.Add(this.rbDate);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(433, 256);
            this.panel3.TabIndex = 1;
            // 
            // rbStatus
            // 
            this.rbStatus.AutoSize = true;
            this.rbStatus.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbStatus.Location = new System.Drawing.Point(194, 61);
            this.rbStatus.Name = "rbStatus";
            this.rbStatus.Size = new System.Drawing.Size(70, 25);
            this.rbStatus.TabIndex = 7;
            this.rbStatus.TabStop = true;
            this.rbStatus.Text = "Status";
            this.rbStatus.UseVisualStyleBackColor = true;
            this.rbStatus.CheckedChanged += new System.EventHandler(this.rbStatus_CheckedChanged);
            // 
            // rbStdId
            // 
            this.rbStdId.AutoSize = true;
            this.rbStdId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbStdId.Location = new System.Drawing.Point(88, 61);
            this.rbStdId.Name = "rbStdId";
            this.rbStdId.Size = new System.Drawing.Size(100, 25);
            this.rbStdId.TabIndex = 6;
            this.rbStdId.TabStop = true;
            this.rbStdId.Text = "Student ID";
            this.rbStdId.UseVisualStyleBackColor = true;
            this.rbStdId.CheckedChanged += new System.EventHandler(this.rbStdId_CheckedChanged);
            // 
            // rbDate
            // 
            this.rbDate.AutoSize = true;
            this.rbDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDate.Location = new System.Drawing.Point(22, 61);
            this.rbDate.Name = "rbDate";
            this.rbDate.Size = new System.Drawing.Size(60, 25);
            this.rbDate.TabIndex = 5;
            this.rbDate.TabStop = true;
            this.rbDate.Text = "Date";
            this.rbDate.UseVisualStyleBackColor = true;
            this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Group By:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.ContextMenuStrip = this.refreshSearch;
            this.panel4.Controls.Add(this.txtSearchValue);
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.cmbColumn);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(498, 256);
            this.panel4.TabIndex = 2;
            // 
            // refreshSearch
            // 
            this.refreshSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RefreshtoolStripMenuItem1});
            this.refreshSearch.Name = "refreshAddRubric";
            this.refreshSearch.Size = new System.Drawing.Size(158, 30);
            // 
            // RefreshtoolStripMenuItem1
            // 
            this.RefreshtoolStripMenuItem1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.RefreshtoolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.RefreshtoolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("RefreshtoolStripMenuItem1.Image")));
            this.RefreshtoolStripMenuItem1.Name = "RefreshtoolStripMenuItem1";
            this.RefreshtoolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.RefreshtoolStripMenuItem1.Size = new System.Drawing.Size(157, 26);
            this.RefreshtoolStripMenuItem1.Text = "Refresh";
            this.RefreshtoolStripMenuItem1.Click += new System.EventHandler(this.RefreshtoolStripMenuItem1_Click);
            // 
            // txtSearchValue
            // 
            this.txtSearchValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchValue.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchValue.Location = new System.Drawing.Point(179, 111);
            this.txtSearchValue.Name = "txtSearchValue";
            this.txtSearchValue.Size = new System.Drawing.Size(246, 29);
            this.txtSearchValue.TabIndex = 13;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.btnSearch.Location = new System.Drawing.Point(64, 190);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(361, 37);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(59, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 3;
            this.label10.Text = "Value:";
            // 
            // cmbColumn
            // 
            this.cmbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbColumn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbColumn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.cmbColumn.FormattingEnabled = true;
            this.cmbColumn.Items.AddRange(new object[] {
            "AttendanceId",
            "StudentId",
            "AttendanceStatus"});
            this.cmbColumn.Location = new System.Drawing.Point(175, 49);
            this.cmbColumn.Name = "cmbColumn";
            this.cmbColumn.Size = new System.Drawing.Size(250, 28);
            this.cmbColumn.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(54, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Column Name:";
            // 
            // view
            // 
            this.view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.view.AutoSize = true;
            this.view.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Location = new System.Drawing.Point(885, 314);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(66, 21);
            this.view.TabIndex = 15;
            this.view.Text = "View All";
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dtViewStudentAttendance);
            this.panel2.Location = new System.Drawing.Point(2, 356);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(982, 273);
            this.panel2.TabIndex = 2;
            // 
            // dtViewStudentAttendance
            // 
            this.dtViewStudentAttendance.AllowUserToAddRows = false;
            this.dtViewStudentAttendance.AllowUserToResizeColumns = false;
            this.dtViewStudentAttendance.AllowUserToResizeRows = false;
            this.dtViewStudentAttendance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtViewStudentAttendance.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtViewStudentAttendance.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewStudentAttendance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtViewStudentAttendance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewStudentAttendance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtViewStudentAttendance.ColumnHeadersHeight = 35;
            this.dtViewStudentAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtViewStudentAttendance.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtViewStudentAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtViewStudentAttendance.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtViewStudentAttendance.Location = new System.Drawing.Point(0, 0);
            this.dtViewStudentAttendance.Name = "dtViewStudentAttendance";
            this.dtViewStudentAttendance.ReadOnly = true;
            this.dtViewStudentAttendance.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtViewStudentAttendance.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtViewStudentAttendance.RowHeadersVisible = false;
            this.dtViewStudentAttendance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtViewStudentAttendance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dtViewStudentAttendance.Size = new System.Drawing.Size(982, 273);
            this.dtViewStudentAttendance.TabIndex = 19;
            // 
            // rbMonthWise
            // 
            this.rbMonthWise.AutoSize = true;
            this.rbMonthWise.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMonthWise.Location = new System.Drawing.Point(22, 104);
            this.rbMonthWise.Name = "rbMonthWise";
            this.rbMonthWise.Size = new System.Drawing.Size(158, 25);
            this.rbMonthWise.TabIndex = 8;
            this.rbMonthWise.TabStop = true;
            this.rbMonthWise.Text = "Month Wise Status";
            this.rbMonthWise.UseVisualStyleBackColor = true;
            this.rbMonthWise.CheckedChanged += new System.EventHandler(this.rbMonthWise_CheckedChanged);
            // 
            // rbMontWiseEachStudent
            // 
            this.rbMontWiseEachStudent.AutoSize = true;
            this.rbMontWiseEachStudent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMontWiseEachStudent.Location = new System.Drawing.Point(22, 142);
            this.rbMontWiseEachStudent.Name = "rbMontWiseEachStudent";
            this.rbMontWiseEachStudent.Size = new System.Drawing.Size(251, 25);
            this.rbMontWiseEachStudent.TabIndex = 9;
            this.rbMontWiseEachStudent.TabStop = true;
            this.rbMontWiseEachStudent.Text = "Month Wise Each Student Status";
            this.rbMontWiseEachStudent.UseVisualStyleBackColor = true;
            this.rbMontWiseEachStudent.CheckedChanged += new System.EventHandler(this.rbMontWiseEachStudent_CheckedChanged);
            // 
            // rbPercentage
            // 
            this.rbPercentage.AutoSize = true;
            this.rbPercentage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPercentage.Location = new System.Drawing.Point(22, 180);
            this.rbPercentage.Name = "rbPercentage";
            this.rbPercentage.Size = new System.Drawing.Size(248, 25);
            this.rbPercentage.TabIndex = 10;
            this.rbPercentage.TabStop = true;
            this.rbPercentage.Text = "Monthly Attendance Percentage";
            this.rbPercentage.UseVisualStyleBackColor = true;
            this.rbPercentage.CheckedChanged += new System.EventHandler(this.rbPercentage_CheckedChanged);
            // 
            // rbDailyAttendancePers
            // 
            this.rbDailyAttendancePers.AutoSize = true;
            this.rbDailyAttendancePers.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDailyAttendancePers.Location = new System.Drawing.Point(22, 217);
            this.rbDailyAttendancePers.Name = "rbDailyAttendancePers";
            this.rbDailyAttendancePers.Size = new System.Drawing.Size(260, 25);
            this.rbDailyAttendancePers.TabIndex = 11;
            this.rbDailyAttendancePers.TabStop = true;
            this.rbDailyAttendancePers.Text = "Date Wise Attendance Percentage";
            this.rbDailyAttendancePers.UseVisualStyleBackColor = true;
            this.rbDailyAttendancePers.CheckedChanged += new System.EventHandler(this.rbDailyAttendancePers_CheckedChanged);
            // 
            // ucViewAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucViewAttendance";
            this.Size = new System.Drawing.Size(984, 632);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.refreshSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchValue)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtViewStudentAttendance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label view;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dtViewStudentAttendance;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbColumn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip refreshSearch;
        private System.Windows.Forms.ToolStripMenuItem RefreshtoolStripMenuItem1;
        private System.Windows.Forms.RadioButton rbStatus;
        private System.Windows.Forms.RadioButton rbStdId;
        private System.Windows.Forms.RadioButton rbDate;
        private System.Windows.Forms.NumericUpDown txtSearchValue;
        private System.Windows.Forms.RadioButton rbMontWiseEachStudent;
        private System.Windows.Forms.RadioButton rbMonthWise;
        private System.Windows.Forms.RadioButton rbPercentage;
        private System.Windows.Forms.RadioButton rbDailyAttendancePers;
    }
}

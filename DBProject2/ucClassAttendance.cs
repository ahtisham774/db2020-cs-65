﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucClassAttendance : UserControl
    {
        private static ucClassAttendance _instance;
        public static ucClassAttendance Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucClassAttendance();
                }
                return _instance;
            }
        }
        private ucClassAttendance()
        {
            InitializeComponent();
        }
        private bool isFoundDate(String date)
        {
            DataTable table = Search(date);
            if (table.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }
        private void btnAddRubric_Click(object sender, EventArgs e)
        {
            String date = dClassAttendance.Value.ToShortDateString();
            if (isFoundDate(date))
            {
                MessageBox.Show("Already Enterd this date");
            }
            else
            {
                Insert(date);
                dtViewClassAttendance.DataSource = Search("");
            }

        }
        private void Insert(String date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO ClassAttendance VALUES( '"+date+"')",con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        public DataTable Search(String date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;
            if(date != "")
            {
                cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE AttendanceDate = '" + date + "'", con);
            }
            else
            {
                cmd = new SqlCommand("SELECT * FROM ClassAttendance", con);
            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE id LIKE '%" + str +"%' OR AttendanceDate LIKE '%" + str + "%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        private void view_Click(object sender, EventArgs e)
        {
            dtViewClassAttendance.DataSource = Search("");
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String str = txtSearch.Text;
            dtViewClassAttendance.DataSource = IsContains(str);
        }
        public void getClassAttendances(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT AttendanceDate FROM ClassAttendance", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getClassAttendanceIndex(String date)
        {
            String index = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM ClassAttendance WHERE AttendanceDate = '" + date + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            index = dt.Rows[0].ItemArray[0].ToString();
            return index;
        }
        public String getClassAttendanceDate(String index)
        {
           
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT AttendanceDate FROM ClassAttendance WHERE id = " + index, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
            
        }

        private void RefreshtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dClassAttendance.Value = DateTime.Now;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucAssessmentComponent : UserControl
    {
        private static ucAssessmentComponent _instance;
        private static String assessmentComponentIndex = "";
        public static ucAssessmentComponent Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ucAssessmentComponent();
                }
                return _instance;
            }
        }
        private ucAssessmentComponent()
        {
            InitializeComponent();
        }

        private void refreshAddAssessmentComp_Click(object sender, EventArgs e)
        {
            Refresh("Add");
        }

        private void searchAssessmentComp_Click(object sender, EventArgs e)
        {
            Refresh("Search");
        }

        private void btnAddAssessmentComponent_Click(object sender, EventArgs e)
        {
            string name = "";
            if (txtName.Text != "")
            {
                name = txtName.Text;
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String totalMarks = "";
            if (numTotalMarks.Value != 0)
            {
                totalMarks = numTotalMarks.Value.ToString();
            }
            else
            {
                MessageBox.Show("Please Fill the Field");
            }
            String rubricId = "";
            if (cmbRubricId.SelectedIndex != -1)
            {
                String rubricName = cmbRubricId.SelectedItem.ToString();
                rubricId = ucRubric.Instance.getRubricIndex(rubricName);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            String assessmentId = "";
            if (cmbRubricId.SelectedIndex != -1)
            {
                String assessmentName = cmbAssessmentId.SelectedItem.ToString();
                assessmentId = ucAssessment.Instance.getAssessmentIndex(assessmentName);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
            if (name != "" && totalMarks != "" && rubricId != "")
            {
                if (btnAddAssessmentComponent.Text == "Add")
                {
                    var cdate = DateTime.Now.ToShortDateString();
                    var update = DateTime.Now.ToShortDateString();
                    Insert(name, totalMarks, rubricId, assessmentId, cdate,update);
                }
                if (btnAddAssessmentComponent.Text == "Update")
                {
                    var update = DateTime.Now.ToShortDateString();
                    Update(name, totalMarks, rubricId, assessmentId,update,assessmentComponentIndex);
                }
                dtViewAssessmentComponent.DataSource = Search("", "");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String column = "";
            String value = "";
            if (cmbColumn.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Column first to search");
            }
            else
            {
                column = cmbColumn.SelectedItem.ToString();
            }
            if (txtSearchValue.Text == "")
            {
                MessageBox.Show("Please Enter Value to Search");
            }
            else
            {
                value = txtSearchValue.Text;
                dtViewAssessmentComponent.DataSource = Search(column, value);
            }
        }

        private void view_Click(object sender, EventArgs e)
        {
            dtViewAssessmentComponent.DataSource =  Search("", "");
        }
        private void Insert(String name, String totalMarks, String rubricId, String assessmentId, String cDate, String upDate)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO AssessmentComponent VALUES('" + name + "','" + rubricId + "'," + totalMarks + ",'" + cDate + "', '"+ upDate + "', " + assessmentId + ")", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Inserted");
        }
        private void Update(String name, String totalMarks, String rubricId, String assessmentId,String date, String index)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE AssessmentComponent SET Name = '" + name + "', TotalMarks = " + totalMarks + ", RubricId = " + rubricId + ", AssessmentId = " + assessmentId + ", DateUpdated = '" + date + "' WHERE id = " + index, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data is Updated");
        }
        public DataTable Search(String Column, String val)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd;

            if (Column != "")
            {

                cmd = new SqlCommand("SELECT id,Name,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id),TotalMarks,DateCreated,DateUpdated,AssessmentId = (SELECT Title FROM Assessment WHERE AssessmentId = id) FROM AssessmentComponent WHERE " + Column + "= '" + val + "'", con);

            }
            else
            {

                cmd = new SqlCommand("SELECT id,Name,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id),TotalMarks,DateCreated,DateUpdated,AssessmentId = (SELECT Title FROM Assessment WHERE AssessmentId = id) FROM AssessmentComponent", con);


            }
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private void Refresh(String option)
        {
            if (option == "Add")
            {
                txtName.Text = "";
                cmbRubricId.Text = "";
                cmbRubricId.SelectedIndex = -1; 
                cmbAssessmentId.Text = "";
                cmbAssessmentId.SelectedIndex = -1;
                numTotalMarks.Value = 0;
                btnAddAssessmentComponent.Text = "Add";
            }
            if (option == "Search")
            {
                cmbColumn.SelectedIndex = -1;
                cmbColumn.Text = "";
                txtSearchValue.Text = "";
            }
        }
        private DataTable IsContains(String str)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id,Name,RubricId = (SELECT Details FROM Rubric WHERE RubricId = id),TotalMarks,DateCreated,DateUpdated,AssessmentId = (SELECT Title FROM Assessment WHERE AssessmentId = id) FROM AssessmentComponent WHERE id LIKE '%" + str + "%' OR Name LIKE '%" + str + "%' OR TotalMarks LIKE '%" + str + "%' OR (SELECT Details FROM Rubric WHERE RubricId = id) LIKE '%" + str + "%' OR DateCreated LIKE '%" + str + "%' OR DateUpdated LIKE '%" + str + "%' OR (SELECT Title FROM Assessment WHERE AssessmentId = id) LIKE '%" + str+"%'", con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            apt.Fill(dt);
            return dt;
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String str = txtSearch.Text;
            dtViewAssessmentComponent.DataSource = IsContains(str);
        }
        public void getAssessmentIds()
        {
            cmbAssessmentId.Items.Clear();
            ucAssessment.Instance.getAssessements(cmbAssessmentId);
        }
        public void getRubricIds()
        {
            cmbRubricId.Items.Clear();
            ucRubric.Instance.getRubrics(cmbRubricId);
        }

        private void dtViewAssessmentComponent_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dt = dtViewAssessmentComponent.Rows[0];
            String index = "";
            try
            {
                dt = dtViewAssessmentComponent.Rows[e.RowIndex];
                index = dt.Cells[0].Value.ToString();
            }
            catch (Exception ex) { }
            if (index != "")
            {
                txtName.Text = dt.Cells[1].Value.ToString();
                cmbRubricId.SelectedItem = dt.Cells[2].Value.ToString();
                cmbAssessmentId.SelectedItem = dt.Cells[6].Value.ToString();
                numTotalMarks.Value = Decimal.Parse(dt.Cells[3].Value.ToString());
                assessmentComponentIndex = index;
                btnAddAssessmentComponent.Text = "Update";
            }
        }
        public void getAssessmentComponents(ComboBox item)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM AssessmentComponent", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            foreach (DataRow col in dt.Rows)
            {
                item.Items.Add(col[0].ToString());

            }

        }
        public String getAssessmentComponentValue(String index)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM AssessmentComponent WHERE id = " + index, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
        public String getAssessmentComponentIndex(String val)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT id FROM AssessmentComponent WHERE Name = '" + val + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
        }
    }
}

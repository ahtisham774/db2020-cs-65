﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucStudentAttendance : UserControl
    {
        private static ucStudentAttendance _instance;
        private DataGridViewCellEventArgs location;
       
        public static ucStudentAttendance Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucStudentAttendance();
                }
                return _instance;
            }
        }
        private ucStudentAttendance()
        {
            InitializeComponent();
            marksAttendance();
        }

        private void btnAddStudentAttendance_Click(object sender, EventArgs e)
        {
            String attendanceId = "";
            String attendanceDate = "";
            if (cmbAttendanceId.SelectedIndex != -1)
            {
                attendanceDate = cmbAttendanceId.SelectedItem.ToString();
                attendanceId = ucClassAttendance.Instance.getClassAttendanceIndex(attendanceDate);

            }
            else
            {
                MessageBox.Show("Please Select the Field");
            }
          

            if (attendanceId != "" )
            {
                if (btnAddStudentAttendance.Text == "Add")
                {
                    if (isFindAttendanceId(attendanceId))
                    {
                        DialogResult result =  MessageBox.Show("Attendance of this date is Already marked. \n If you want to Update then press Enter.","Confirmation",MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                        if(result == DialogResult.Yes)
                        {

                            UpdateAttendance(attendanceId);
                           
                            MessageBox.Show("Attendance is Updated");
                            Refresh();
                        }
                    }
                    else
                    {
                        FinalAttendanceWithIndices(attendanceId);
                        MessageBox.Show("Attendance is Marked");
                        Refresh();
                    }


                }
                

            }
        }

        //private void btnSearch_Click(object sender, EventArgs e)
        //{

        //}
        private bool isFindAttendanceId(String date)
        {
            DataTable table = ucViewAttendance.Instance.Search("AttendanceId", date);
            if(table.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }
        public void getAttendaces()
        {
            cmbAttendanceId.Items.Clear();
            ucClassAttendance.Instance.getClassAttendances(cmbAttendanceId);
        }

        private void marksAttendance()
        {

            dtMarksAttendance.DataSource = getRegistrationAndStatus();
        }
        private DataTable getRegistrationAndStatus()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT RegistrationNumber,Look.Name FROM Student CROSS JOIN (SELECT TOP(1) Name FROM Lookup) AS Look", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt;
        }
        private void presentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dtMarksAttendance.Rows[location.RowIndex].Cells[1].Value = "Present";
            }
            catch (Exception) { }
        }

        private void dtMarksAttendance_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            location = e;
        }

        private void absentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dtMarksAttendance.Rows[location.RowIndex].Cells[1].Value = "Absent";
            }
            catch (Exception) { }
        }

        private void leaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dtMarksAttendance.Rows[location.RowIndex].Cells[1].Value = "Leave";
            }
            catch (Exception) { }
        }

        private void lateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dtMarksAttendance.Rows[location.RowIndex].Cells[1].Value = "Late";
            }
            catch (Exception) { }
        }

        //private void btnSearch_Click(object sender, EventArgs e)
        //{

        //}
        public String getStatusIndex(String status)
        {
            String index = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT LookupId FROM Lookup WHERE Name = '" + status + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            index = dt.Rows[0].ItemArray[0].ToString();
            return index;
        }
        public String getStatusValue(String index)
        {
            
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Lookup WHERE LookupId = " + index, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            return dt.Rows[0].ItemArray[0].ToString();
            
        }
        private void FinalAttendanceWithIndices(String attendanceId)
        {
            DataGridView dt = dtMarksAttendance;
      
            for (int i = 0; i<dt.Rows.Count; i++)
            {
                String stdId = ucStudent.Instence.getStudentIndex(dt.Rows[i].Cells[0].Value.ToString());
                String status = getStatusIndex(dt.Rows[i].Cells[1].Value.ToString());

                Insert(attendanceId, stdId, status);
            }
            
        }
        private void Insert(String attendanceId, String studentId, String status)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentAttendance VALUES("+attendanceId+", "+studentId+", "+status+")",con);
            cmd.ExecuteNonQuery();
        }
        private void Update(String attendanceId, String status)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE StudentAttendance SET  AttendanceStatus = '" + status + "' WHERE AttendanceId = " + attendanceId, con);
            cmd.ExecuteNonQuery();
            
        }
        private void UpdateAttendance(String attendanceId)
        {
            DataGridView dview = dtMarksAttendance;
            foreach(DataGridViewRow row in dview.Rows)
            {
                Update(attendanceId, getStatusIndex(row.Cells[1].Value.ToString()));
            }
        }
        private void RefreshtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cmbAttendanceId.SelectedIndex = -1;
            cmbAttendanceId.Text = "";
            marksAttendance();
        }
    }
}

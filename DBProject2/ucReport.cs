﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject2
{
    public partial class ucReport : UserControl
    {
        private static ucReport _instance;
        public static ucReport Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucReport();

                }
                return _instance;
            }
        }
        private ucReport()
        {
            InitializeComponent();
            lbSave.Visible = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            String name = Get(fileSave, "Clo");
            if(name!= "")
            {
                GenerateReport.generateCLOWiseReport(name, "CLO Wise Class Report ");
                DialogResult result =  MessageBox.Show("File is Generated \n Do you wants to open it with your default appliction ?", "Question",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if(result == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(name);
                }
                
            }
            
            
        }
        private String Get(SaveFileDialog file,String defaulName)
        {
            file.FileName = defaulName;
            file.Title = "Save "+defaulName;
            file.DefaultExt = "pdf";
            file.AddExtension = true;
            DriveInfo[] info = DriveInfo.GetDrives();
            file.InitialDirectory = info[1].Name;
            file.CreatePrompt = true;
            file.OverwritePrompt = true;
            file.RestoreDirectory = true;
            file.Filter = "pdf files (*.pdf)|*.pdf";
            file.FilterIndex = 2;
            if (file.ShowDialog() == DialogResult.OK)
            {
                lbSave.Text = file.FileName;
                lbSave.Show();
                return file.FileName;
            }
            return "";
        }
        private void btnAssessmentResult_Click(object sender, EventArgs e)
        {
            String name = Get(fileSave, "Assessment");
            if(name != "")
            {
                GenerateReport.generateAssessmentWiseReport(name,"Assessment Wise Class Report");
                DialogResult result = MessageBox.Show("File is Generated \n Do you wants to open it with your default appliction ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(name);
                }
            }
        }
        public void getAssessments()
        {
            cmbAssessmentId.Items.Clear();
            ucAssessment.Instance.getAssessements(cmbAssessmentId);
        }

        private void btnAssessmentReport_Click(object sender, EventArgs e)
        {
            String type = "";
            if(cmbAssessmentId.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select item From box");
            }
            else
            {
                type = cmbAssessmentId.SelectedItem.ToString();
                String name = Get(fileSave, type);
                if (name != "")
                {
                    GenerateReport.generateStudentAttendAssessment(name,"Which Student Attends "+type+" Or Not",type);
                    DialogResult result = MessageBox.Show("File is Generated \n Do you wants to open it with your default appliction ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(name);
                    }
                }
            }
            
        }

        private void RefreshtoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            lbSave.Text = "";
            cmbAssessmentId.SelectedItem = "";
            cmbAssessmentId.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
    
            String name = Get(fileSave,"Rubrics");
            if (name != "")
            {
                GenerateReport.RubricsReport(name, "Rubrics Of Each Assessment");
                DialogResult result = MessageBox.Show("File is Generated \n Do you wants to open it with your default appliction ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(name);
                }
            }

        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
            String name = Get(fileSave, "Attendance");
            if (name != "")
            {
                GenerateReport.AttendaceReport(name, "Attendancce");
                DialogResult result = MessageBox.Show("File is Generated \n Do you wants to open it with your default appliction ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(name);
                }
            }

        }
    }
}
